from django.db.models import Case, Exists, OuterRef, QuerySet, Value, When

from posts.choices import PostVoteChoice
from users.models import UserPublic


class PostQuerySet(QuerySet):
    def with_ratings(self, viewer: UserPublic):
        from posts.models import PostVote

        queryset = self.all()

        if not viewer or not viewer.is_authenticated:
            return queryset

        queryset = queryset.annotate(
            _voted=Case(
                When(
                    Exists(
                        PostVote.objects.filter(
                            post=OuterRef("pk"), user=viewer, value=PostVoteChoice.UPVOTE
                        )
                    ),
                    then=Value(PostVoteChoice.UPVOTE),
                ),
                When(
                    Exists(
                        PostVote.objects.filter(
                            post=OuterRef("pk"),
                            user=viewer,
                            value=PostVoteChoice.DOWNVOTE,
                        )
                    ),
                    then=Value(PostVoteChoice.DOWNVOTE),
                ),
            )
        )
        return queryset
