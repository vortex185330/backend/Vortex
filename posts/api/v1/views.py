import uuid

import reversion
from django.conf import settings
from django.db import IntegrityError
from django.http import Http404
from django.utils import timezone
from drf_spectacular.utils import extend_schema
from rest_access_policy import AccessViewSetMixin
from rest_framework import mixins, status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ModelViewSet, ReadOnlyModelViewSet

from comments.services import update_subscriptions_feed_viewed_at
from common.api.helpers import get_formatted_error_response
from common.api.pagination import KapibaraCursorPagination
from posts.api.filters import PostFilter, PostFilterBackend
from posts.api.policies import PostAccessPolicy, PostSeriesAccessPolicy
from posts.api.serializers import (
    AnnouncementSerializer,
    BulkPostViewSerializer,
    PostCreateSerializer,
    PostPublishSerializer,
    PostRatingOnlySerializer,
    PostSaveSerializer,
    PostSerializer,
    PostSeriesSerializer,
    PostVoteBayanCreateSerializer,
    PostVoteCreateSerializer,
)
from posts.choices import PostFeedType, PostStatus
from posts.models import Announcement, Post, PostSeries, PostSeriesMembership, PostView
from posts.selectors import fetch_all_posts, fetch_posts, get_user_restricted_tag_names
from posts.services import (
    delete_post,
    publish_post,
    record_saving_for_post,
    record_vote_bayan_for_post,
    record_vote_for_post,
)
from posts.tasks import record_bulk_views_task, record_post_view_task
from users.models import UserPublic
from users.selectors import (
    can_create_draft_post,
    can_publish_post,
    can_vote,
    can_vote_bayan,
    is_user_autobanned,
)


def replace_content(serializer: PostSerializer) -> PostSerializer:
    first_block_id = serializer.data["content"]["blocks"][0]["id"]
    serializer.data["content"]["blocks"] = [
        {
            "id": first_block_id,
            "data": {"text": settings.DEFAULT_MESSAGE_RESTRICTED_CONTENT},
            "type": "paragraph",
        }
    ]
    return serializer


class PostViewSet(AccessViewSetMixin, ModelViewSet):
    """API для управления постами."""

    serializer_class = PostSerializer
    lookup_field = "slug"
    access_policy = PostAccessPolicy
    pagination_class = KapibaraCursorPagination
    filter_backends = [PostFilterBackend]
    filterset_class = PostFilter

    def get_queryset(self):
        if self.action in ("publish", "update", "partial_update", "destroy"):
            return Post.objects.exclude(status=PostStatus.DELETED)
        if self.action == "retrieve":
            return fetch_all_posts(self.request.user)

        return fetch_posts(self.request.user)

    def get_serializer_class(self):
        """Return proper serializer based on action user performing."""
        if self.action in ("create", "update", "partial_update"):
            return PostCreateSerializer
        return super().get_serializer_class()

    def get_object(self):
        slug = self.kwargs[self.lookup_url_kwarg or self.lookup_field]

        try:
            uuid.UUID(slug)
            lookup_key = "uuid"
        except (TypeError, ValueError):
            lookup_key = "slug"

        queryset = self.filter_queryset(self.get_queryset())
        obj = get_object_or_404(queryset, **{lookup_key: self.kwargs["slug"]})
        self.check_object_permissions(self.request, obj)
        return obj

    def retrieve(self, request, *args, **kwargs):
        post = self.get_object()
        user = request.user
        if post.status == PostStatus.DELETED or (
            post.user != user and post.status != PostStatus.PUBLISHED
        ):
            raise Http404

        serializer = self.get_serializer(post)
        is_restricted = [
            tag
            for tag in serializer.data["tags"]
            if tag in get_user_restricted_tag_names(user)
        ]
        if is_restricted:
            return Response(replace_content(serializer).data)
            # return Response(data=settings.DEFAULT_MESSAGE_RESTRICTED_CONTENT,
            # status=404)
        if user.is_authenticated:
            record_post_view_task.delay(user_id=user.pk, post_id=post.pk)
        return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        user = request.user
        result = super().list(request, *args, **kwargs)
        if self._is_subscription_feed() and user.is_authenticated:
            update_subscriptions_feed_viewed_at(user)

        return result

    def create(self, request, *args, **kwargs):
        user = request.user
        if not can_create_draft_post(user):
            return Response(
                get_formatted_error_response(
                    code="limit_reached",
                    message="Вы достигли лимита черновиков.",
                ),
                status=status.HTTP_429_TOO_MANY_REQUESTS,
            )

        with reversion.create_revision():
            response = super().create(request, *args, **kwargs)
            reversion.set_user(user)
            reversion.set_comment("Пост создан")
            return response

    @action(
        methods=["POST"],
        detail=True,
        queryset=Post.objects.all(),
        serializer_class=PostPublishSerializer,
        lookup_field="uuid",
        lookup_url_kwarg="slug",
    )
    def publish(self, request, *args, **kwargs):
        """Publish post and make it available on the website."""
        user: UserPublic = request.user
        if is_user_autobanned(user):
            code = "rating_too_low"
            message = "Вы не можете публиковать посты из-за низкого рейтинга"
            return Response(
                get_formatted_error_response(
                    code=code,
                    message=message,
                ),
                status=status.HTTP_429_TOO_MANY_REQUESTS,
            )
        # if user.is_banned:
        #     code = "rating_too_low"
        #     message = "Вы не можете публиковать посты потому что вы заблокированы"
        #     return Response(
        #         get_formatted_error_response(
        #             code=code,
        #             message=message,
        #         ),
        #         status=status.HTTP_429_TOO_MANY_REQUESTS,
        #     )
        if not can_publish_post(user):
            if user.is_verified:
                code = "limit_reached"
                message = "Вы достигли лимита постов в сутки"
            else:
                code = "limit_reached_confirm_telegram"
                message = (
                    "Вы достигли лимита постов в сутки для неверифицированного "
                    "пользователя. Подтвердите свой Телеграм для снятия всех лимитов."
                )
            return Response(
                get_formatted_error_response(
                    code=code,
                    message=message,
                ),
                status=status.HTTP_429_TOO_MANY_REQUESTS,
            )
        post = self.get_object()
        serializer = self.get_serializer(data=PostCreateSerializer(post).data)
        serializer.is_valid(raise_exception=True)
        post.content["blocks"] = serializer.validated_data["content"]["blocks"]
        try:
            post = publish_post(post, user)
        except (RuntimeError, IntegrityError):
            return Response(
                data=get_formatted_error_response(
                    code="slug_generation_error",
                    message="Что-то пошло не так, попробуйте опубликовать еще раз.",
                ),
                status=status.HTTP_400_BAD_REQUEST,
            )
        return Response(self.get_serializer(post).data, status=status.HTTP_200_OK)

    def perform_destroy(self, instance: Post):
        """Delete user's own post."""
        delete_post(instance, self.request.user)

    def perform_update(self, serializer):
        with reversion.create_revision():
            super().perform_update(serializer)
            reversion.set_user(self.request.user)
            reversion.set_comment(
                f"Пост отредактирован (статус {serializer.instance.status})"
            )

    @action(
        methods=["POST"],
        detail=True,
        serializer_class=PostVoteCreateSerializer,
    )
    def vote(self, request, *args, **kwargs):
        """Record vote for the post."""
        user = request.user
        if is_user_autobanned(user):
            code = "rating_too_low"
            message = "Вы не можете голосовать за посты из-за низкого рейтинга"
            return Response(
                get_formatted_error_response(
                    code=code,
                    message=message,
                ),
                status=status.HTTP_429_TOO_MANY_REQUESTS,
            )
        # if user.is_banned:
        #     code = "rating_too_low"
        #     message = "Вы не можете голосовать за посты потому что вы заблокированы"
        #     return Response(
        #         get_formatted_error_response(
        #             code=code,
        #             message=message,
        #         ),
        #         status=status.HTTP_429_TOO_MANY_REQUESTS,
        #     )
        if not can_vote(user):
            if user.is_verified:
                code = "limit_reached"
                message = "Вы достигли лимита голосов за посты в сутки"
            else:
                code = "limit_reached_confirm_telegram"
                message = (
                    "Вы достигли лимита голосов за посты в сутки для "
                    "неверифицированного пользователя. Подтвердите свой Телеграм для "
                    "снятия всех лимитов."
                )
            return Response(
                get_formatted_error_response(
                    code=code,
                    message=message,
                ),
                status=status.HTTP_429_TOO_MANY_REQUESTS,
            )
        post = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        record_vote_for_post(post, request.user, serializer.data["value"])
        # Получаем из базы, используя заданный queryset для того, чтобы все аннотации
        # прикрепились к объекту. post.refresh_from_db() не работает, т.к. нужные
        # аннотации не сохраняются
        post = self.get_object()

        return Response(
            PostRatingOnlySerializer(post).data, status=status.HTTP_201_CREATED
        )

    @action(
        methods=["POST"],
        detail=True,
        serializer_class=PostVoteBayanCreateSerializer,
    )
    def vote_for_bayan(self, request, *args, **kwargs):
        """Record vote for bayan the post."""
        user = request.user
        if is_user_autobanned(user):
            code = "rating_too_low"
            message = "Вы не можете голосовать за посты из-за низкого рейтинга"
            return Response(
                get_formatted_error_response(
                    code=code,
                    message=message,
                ),
                status=status.HTTP_429_TOO_MANY_REQUESTS,
            )
        # if user.is_banned:
        #     code = "rating_too_low"
        #     message = "Вы не можете голосовать за посты потому что вы заблокированы"
        #     return Response(
        #         get_formatted_error_response(
        #             code=code,
        #             message=message,
        #         ),
        #         status=status.HTTP_429_TOO_MANY_REQUESTS,
        #     )
        if not can_vote_bayan(user):
            if user.is_verified:
                code = "limit_reached"
                message = "Вы достигли лимита голосов за баяны постов в сутки"
            else:
                code = "limit_reached_confirm_telegram"
                message = (
                    "Вы достигли лимита голосов за баяны посты в сутки для "
                    "неверифицированного пользователя. Подтвердите свой Телеграм для "
                    "снятия всех лимитов."
                )
            return Response(
                get_formatted_error_response(
                    code=code,
                    message=message,
                ),
                status=status.HTTP_429_TOO_MANY_REQUESTS,
            )
        post = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        record_vote_bayan_for_post(post, request.user)
        # Получаем из базы, используя заданный queryset для того, чтобы все аннотации
        # прикрепились к объекту. post.refresh_from_db() не работает, т.к. нужные
        # аннотации не сохраняются
        post = self.get_object()

        return Response(
            PostRatingOnlySerializer(post).data, status=status.HTTP_201_CREATED
        )

    @action(
        methods=["POST"],
        detail=True,
        serializer_class=PostSaveSerializer,
    )
    def save_post(self, request, *args, **kwargs):
        """Record saved for the post."""
        user = request.user
        if not user.is_authenticated:
            code = "limit_reached"
            message = "Авторизуйтесь, для того что бы сохранять посты."
            return Response(
                get_formatted_error_response(
                    code=code,
                    message=message,
                ),
                status=status.HTTP_401_UNAUTHORIZED,
            )
        post = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        record_saving_for_post(post, request.user)
        # Получаем из базы, используя заданный queryset для того, чтобы все аннотации
        # прикрепились к объекту. post.refresh_from_db() не работает, т.к. нужные
        # аннотации не сохраняются
        post = self.get_object()

        return Response(
            PostRatingOnlySerializer(post).data, status=status.HTTP_201_CREATED
        )

    # @action(detail=True, methods=["POST"], url_path="assign-series")
    # def assign_series(self, request, slug=None):
    #     """Привязать пост к серии."""
    #     post = self.get_object()
    #     series_id = request.data.get("series_id")
    #     series = PostSeries.objects.filter(pk=series_id, user=request.user).first()
    #
    #     if not series:
    #         return Response(
    #             {"detail": "Серия не найдена
    #             или не принадлежит текущему пользователю."},
    #             status=status.HTTP_404_NOT_FOUND,
    #         )
    #
    #     post.series = series
    #     post.save()
    #
    #     return Response(
    #         {"detail": f"Пост {post.title} добавлен в серию {series.name}."},
    #         status=status.HTTP_200_OK,
    #     )
    #
    # @action(detail=True, methods=["POST"], url_path="remove-series")
    # def remove_series(self, request, slug=None):
    #     """Удалить пост из серии."""
    #     post = self.get_object()
    #
    #     if not post.series:
    #         return Response(
    #             {"detail": "Пост не принадлежит ни одной серии."},
    #             status=status.HTTP_400_BAD_REQUEST,
    #         )
    #
    #     post.series = None
    #     post.save()
    #
    #     return Response(
    #         {"detail": f"Пост {post.title} удалён из серии."}, status=status.HTTP_200_OK
    #     )

    def _is_subscription_feed(self):
        return self.request.query_params.get("feed_type") == PostFeedType.SUBSCRIPTIONS


class BulkPostViewViewSet(GenericViewSet, mixins.CreateModelMixin):
    queryset = PostView.objects.all()
    http_method_names = ["post"]
    serializer_class = BulkPostViewSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        record_bulk_views_task.delay(
            bulk_views_data=serializer.data["views"], user_id=self.request.user.pk
        )

    @extend_schema(summary="Сохранение просмотра множества постов")
    def create(self, request, *args, **kwargs):
        super().create(request, *args, **kwargs)
        return Response(status=status.HTTP_201_CREATED)


class AnnouncementViewSet(ReadOnlyModelViewSet):
    """ViewSet для управления актуальными постами."""

    queryset = Announcement.objects.all()
    serializer_class = AnnouncementSerializer

    def get_queryset(self):
        """Фильтруем только активные и актуальные объявления."""
        current_time = timezone.now()
        return self.queryset.filter(
            is_active=True, start_date__lte=current_time, end_date__gte=current_time
        ).order_by("priority", "start_date")


class PostSeriesViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin, GenericViewSet):
    """ViewSet для управления сериями постов."""

    queryset = PostSeries.objects.all().order_by("-created_at")
    serializer_class = PostSeriesSerializer
    access_policy = PostSeriesAccessPolicy

    @extend_schema(summary="Создать новую серию постов пользователя")
    def perform_create(self, serializer):
        """Создание новой серии с привязкой к текущему пользователю."""
        serializer.save(user=self.request.user)

    @extend_schema(summary="Удалить серию")
    def perform_destroy(self, instance):
        """Удаление серии с сохранением постов."""
        # Убираем привязку постов к серии перед удалением
        instance.posts.update(series=None)
        super().perform_destroy(instance)

    @extend_schema(summary="Получить все серии пользователя")
    @action(detail=False, methods=["GET"], url_path="by-user/(?P<username>[^/.]+)")
    def get_series_by_user(self, request, username=None):
        """
        Возвращает все серии постов для указанного пользователя.
        """
        user = UserPublic.objects.filter(username=username).first()
        if not user:
            raise Http404({"detail": "Пользователь не найден."})

        series = self.queryset.filter(user=user)
        serializer = self.get_serializer(series, many=True)
        return Response(serializer.data)

    @extend_schema(summary="Редактировать серию")
    def update(self, request, *args, **kwargs):
        """Частичное или полное обновление серии."""
        partial = kwargs.pop("partial", False)  # Проверяем, используется ли PATCH
        instance = self.get_object()  # Получаем объект, который нужно обновить
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    def perform_update(self, serializer):
        """Логика обновления объекта."""
        serializer.save()

    @extend_schema(summary="Добавить пост в серию")
    @action(detail=True, methods=["POST"], url_path="add-to-series")
    def add_to_series(self, request, slug=None):
        """Добавить пост в серию."""
        post = self.get_object()
        if post.status != PostStatus.PUBLISHED:
            return Response(
                {"detail": "Добавлять в серию можно только опубликованные посты."},
                status=status.HTTP_404_NOT_FOUND,
            )
        series_id = request.data.get("series_id")
        series = PostSeries.objects.filter(pk=series_id, user=request.user).first()

        if not series:
            return Response(
                {
                    "detail": "Серия не найдена или не принадлежит "
                    "текущему пользователю."
                },
                status=status.HTTP_404_NOT_FOUND,
            )

        # Проверяем, не добавлен ли уже пост в серию
        if PostSeriesMembership.objects.filter(post=post, series=series).exists():
            return Response(
                {"detail": "Пост уже добавлен в эту серию."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        # Добавляем пост в серию
        PostSeriesMembership.objects.create(post=post, series=series)
        return Response(
            {"detail": f"Пост '{post.title}' добавлен в серию '{series.name}'."},
            status=status.HTTP_201_CREATED,
        )

    @extend_schema(summary="Удалить пост из серии")
    @action(detail=True, methods=["POST"], url_path="remove-from-series")
    def remove_from_series(self, request, slug=None):
        """Удалить пост из серии."""
        post = self.get_object()
        series_id = request.data.get("series_id")
        series = PostSeries.objects.filter(pk=series_id, user=request.user).first()

        if not series:
            return Response(
                {
                    "detail": "Серия не найдена или "
                    "не принадлежит текущему пользователю."
                },
                status=status.HTTP_404_NOT_FOUND,
            )

        membership = PostSeriesMembership.objects.filter(post=post, series=series).first()
        if not membership:
            return Response(
                {"detail": "Пост не состоит в указанной серии."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        # Удаляем связь поста с серией
        membership.delete()
        return Response(
            {"detail": f"Пост '{post.title}' удалён из серии '{series.name}'."},
            status=status.HTTP_200_OK,
        )

    @extend_schema(summary="Удалить серию")
    @action(detail=True, methods=["DELETE"], url_path="delete-series")
    def delete_series(self, request, pk=None):
        """Удалить серию постов."""
        series = self.get_object()

        # Убираем все посты из серии
        memberships = PostSeriesMembership.objects.filter(series=series)
        memberships.delete()

        # Удаляем саму серию
        series.delete()

        return Response(
            {"detail": f"Серия '{series.name}' была успешно удалена."},
            status=status.HTTP_204_NO_CONTENT,
        )

    # @action(detail=False, methods=["POST"], url_path="update-series-order")
    # def update_series_order(self, request):
    #     """Обновить порядок постов в серии."""
    #     series_id = request.data.get("series_id")
    #     post_orders = request.data.get("post_orders", [])
    #
    #     series = PostSeries.objects.filter(pk=series_id,
    #     user=request.user).first()
    #     if not series:
    #         return Response(
    #             {"detail": "Серия не найдена или не
    #             принадлежит текущему пользователю."},
    #             status=status.HTTP_404_NOT_FOUND,
    #         )
    #
    #     # Проверяем, что все посты существуют и
    #     принадлежат текущему пользователю
    #     post_ids = [item["post_id"] for item in post_orders]
    #     posts = Post.objects.filter(pk__in=post_ids, user=request.user)
    #     if posts.count() != len(post_ids):
    #         return Response(
    #             {"detail": "Некоторые посты не найдены
    #             или не принадлежат текущему пользователю."},
    #             status=status.HTTP_400_BAD_REQUEST,
    #         )
    #
    #     # Обновляем порядок постов в серии
    #     for item in post_orders:
    #         PostSeriesMembership.objects.filter(
    #             series=series, post_id=item["post_id"]
    #         ).update(added_at=item["order"])
    #
    #     return Response(
    #         {"detail": f"Порядок постов в серии '{series.name}' обновлён."},
    #         status=status.HTTP_200_OK,
    #     )
