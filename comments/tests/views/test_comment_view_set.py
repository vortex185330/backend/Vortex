import datetime
import uuid
from unittest.mock import ANY

import pytest
from django.conf import settings
from freezegun import freeze_time
from funcy import first, lpluck, pluck, pluck_attr
from rest_framework import status
from reversion.models import Version

from comments.choices import CommentVoteChoice
from comments.models import Comment
from comments.tests.factories import CommentFactory, CommentVoteFactory
from comments.tests.mixins import CommentApiActionsMixin
from posts.choices import PostStatus
from posts.tests.factories import PostFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestCommentViewSet(CommentApiActionsMixin):
    def setup_method(self):
        self.post = PostFactory(status=PostStatus.PUBLISHED)
        self.user = UserPublicFactory()
        self.content_html = {"type": "paragraph", "data": {"text": "test comment"}}
        self.content_image = {
            "type": "image",
            "data": {
                "file": {
                    "url": f"https://{settings.CDN_BASE_DOMAIN}/img.jpeg",
                },
                "caption": "Image caption",
                "withBorder": True,
                "stretched": False,
                "withBackground": True,
            },
        }
        self.content_youtube = {
            "type": "embed",
            "data": {
                "service": "youtube",
                "source": "https://youtube.com",
                "embed": "embed",
                "width": 580,
                "height": 320,
                "caption": "Описание",
            },
        }
        self.content = {
            "blocks": [
                self.content_html,
                self.content_image,
            ],
            "version": "",
        }

    def test_user_own_comments_visible_for_own_user_only(self, authed_api_client):
        user = UserPublicFactory()
        user_comments = CommentFactory.create_batch(size=2, user=user, post=self.post)
        CommentFactory(post=self.post)
        response = self._get_comments(authed_api_client(user), my=True)
        assert response.status_code == status.HTTP_200_OK, response.content.decode()

        result = response.json()
        comments = result["results"]
        assert set(pluck("uuid", comments)) == set(
            map(str, set(pluck_attr("uuid", user_comments)))
        )

        another_user = UserPublicFactory()
        response = self._get_comments(authed_api_client(another_user))
        assert response.status_code == status.HTTP_200_OK, response.content.decode()
        assert len(response.json()) == 0

    def test_user_own_comments_not_visible_for_unauthenticated_user(
        self, anon_api_client
    ):
        client = anon_api_client()
        CommentFactory(post=self.post)
        response = self._get_comments(client, my=True)

        assert response.status_code == status.HTTP_200_OK, response.content.decode()
        assert response.data["results"] == []

    @pytest.mark.parametrize("ordering", ["rating", "-rating"])
    def test_user_own_comments_sorting_by_rating(self, ordering, authed_api_client):
        user_comment_high_rating, user_comment_low_rating = CommentFactory.create_batch(
            size=2, user=self.user, post=self.post
        )

        CommentVoteFactory.create_batch(
            size=2, comment=user_comment_high_rating, value=CommentVoteChoice.UPVOTE
        )
        CommentVoteFactory.create_batch(
            size=2, comment=user_comment_low_rating, value=CommentVoteChoice.DOWNVOTE
        )

        client = authed_api_client(self.user)

        comments_ordering = {
            "rating": [user_comment_low_rating, user_comment_high_rating],
            "-rating": [user_comment_high_rating, user_comment_low_rating],
        }[ordering]

        response = self._get_comments(client, my="true", ordering=ordering)
        assert response.status_code == status.HTTP_200_OK, response.content.decode()

        result = response.json()
        comments = lpluck("uuid", result["results"])

        assert comments == list(map(str, pluck_attr("uuid", comments_ordering)))

    @pytest.mark.parametrize("ordering", ["", "created_at", "-created_at"])
    def test_user_own_comments_sorting_by_date(self, ordering, authed_api_client):
        with freeze_time(datetime.timedelta(days=-2)):
            user_comment_old = CommentFactory(user=self.user)
        user_comment_new = CommentFactory(user=self.user)
        comments_ordering = {
            "": [user_comment_new, user_comment_old],
            "created_at": [user_comment_old, user_comment_new],
            "-created_at": [user_comment_new, user_comment_old],
        }[ordering]

        response = self._get_comments(
            authed_api_client(self.user), my="true", ordering=ordering
        )
        assert response.status_code == status.HTTP_200_OK, response.content.decode()

        result = response.json()
        comments = lpluck("uuid", result["results"])

        assert comments == list(map(str, pluck_attr("uuid", comments_ordering)))

    def test_post_comments_sorted_by_date(self, anon_api_client):
        comments = CommentFactory.create_batch(size=3, post=self.post)
        sorted_comments_uuid = list(
            map(str, pluck_attr("uuid", sorted(comments, key=lambda c: c.created_at)))
        )
        response = self._get_comments(anon_api_client(), post=self.post.slug)
        assert response.status_code == status.HTTP_200_OK, response.content.decode()
        result = response.json()
        assert sorted_comments_uuid == lpluck("uuid", result)

    def test_active_user_can_post_comment(self, authed_api_client):
        result = self._post_comment(
            authed_api_client(self.user), self.post, {"content": self.content}
        )
        data = result.data
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        assert data["author"]["username"] == self.user.username
        assert data["content"] == {**self.content, "time": ANY}

        self.post.refresh_from_db()
        self.user.refresh_from_db()
        assert self.post.comments.count() == 1
        assert self.post.comments_count == 1
        assert self.user.comments.count() == 1
        assert self.user.comments_count == 1

        # Проверка создания версии комментария
        versions = Version.objects.get_for_object(Comment.objects.get(uuid=data["uuid"]))
        assert versions.count() == 1
        version = versions.first()
        assert version.revision.user_id == self.user.id
        assert version.revision.comment == "Комментарий добавлен"
        assert version.field_dict["content"] == {**self.content, "time": ANY}

    @pytest.mark.parametrize(
        "time_between_comments,expected_status_code",
        ((0, status.HTTP_429_TOO_MANY_REQUESTS), (1000, status.HTTP_201_CREATED)),
    )
    def test_post_comment_rate_limit(
        self, time_between_comments, expected_status_code, authed_api_client
    ):
        """Тест для проверки того, что юзер не сможет запостить
        несколько комментов подряд в течении x секунд.
        """
        result = self._post_comment(
            authed_api_client(self.user), self.post, {"content": self.content}
        )
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        with freeze_time(datetime.timedelta(seconds=time_between_comments)):
            result = self._post_comment(
                authed_api_client(self.user), self.post, {"content": self.content}
            )
        assert result.status_code == expected_status_code, result.content.decode()

    def test_post_comment_rate_limit_not_applied_globally(self, authed_api_client):
        """Тест проверяет что рейт-лимит не действует на разных юзеров."""
        for _ in range(2):
            result = self._post_comment(
                authed_api_client(UserPublicFactory()),
                self.post,
                {"content": self.content},
            )
            assert result.status_code == status.HTTP_201_CREATED, result.content.decode()

    def test_edit_comment_not_throttled(self, authed_api_client):
        comment = CommentFactory(post=self.post)
        for _ in range(2):
            result = self._edit_comment(
                authed_api_client(comment.user), comment, data={"content": self.content}
            )
            assert result.status_code == status.HTTP_200_OK

    def test_cannot_comment_with_images_over_the_limit(self, authed_api_client):
        images_content = [self.content_image] * (
            settings.CONTENT_COMMENT_BLOCK_IMG_MAX_ITEMS + 1
        )
        content = self.content.copy()
        content["blocks"] = [*content["blocks"], *images_content]
        result = self._post_comment(
            authed_api_client(self.user), self.post, {"content": content}
        )
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()
        response = result.json()
        assert (
            response["content"][0]
            == f"Можно добавить не более {settings.CONTENT_COMMENT_BLOCK_IMG_MAX_ITEMS} "
            f"изображений"
        )

    def test_cannot_comment_with_videos_over_the_limit(self, authed_api_client):
        videos_content = [self.content_youtube] * (
            settings.CONTENT_COMMENT_BLOCK_VIDEO_MAX_ITEMS + 1
        )
        content = self.content.copy()
        content["blocks"] = [*content["blocks"], *videos_content]
        result = self._post_comment(
            authed_api_client(self.user), self.post, {"content": content}
        )
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()
        response = result.json()
        assert (
            response["content"][0] == "Можно добавить не более "
            f"{settings.CONTENT_COMMENT_BLOCK_VIDEO_MAX_ITEMS} видео"
        )

    def test_cannot_comment_with_text_blocks_over_the_limit(self, authed_api_client):
        html_content = [self.content_html] * (
            settings.CONTENT_COMMENT_BLOCK_MAX_HTML_ITEMS + 1
        )
        content = self.content.copy()
        content["blocks"] = [*content["blocks"], *html_content]
        result = self._post_comment(
            authed_api_client(self.user), self.post, {"content": content}
        )
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()
        response = result.json()
        assert (
            response["content"][0]
            == f"Можно добавить не более {settings.CONTENT_COMMENT_BLOCK_MAX_HTML_ITEMS} "
            f"блоков текста"
        )

    def test_cannot_comment_with_text_length_over_the_limit(self, authed_api_client):
        html_content = {
            "type": "paragraph",
            "data": {
                "text": "*" * (settings.CONTENT_BLOCK_MAX_LENGTH_BYTES + 1),
            },
        }
        content = self.content.copy()
        content["blocks"] = [*content["blocks"], html_content]
        result = self._post_comment(
            authed_api_client(self.user), self.post, {"content": content}
        )
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()
        response = result.json()
        assert (
            response["content"][0] == f"Длина текста должна быть не более "
            f"{settings.CONTENT_BLOCK_MAX_LENGTH_BYTES} символов"
        )

    @pytest.mark.parametrize(
        "content",
        (
            "",
            [],
            "test",
            {"type": "paragraph", "data": {"text": "comment"}},
            [
                {
                    "type": "image",
                    "data": {
                        "file": {
                            "url": "https://external.com/img.jpeg",
                        },
                        "caption": "Image caption",
                        "withBorder": True,
                        "stretched": False,
                        "withBackground": True,
                    },
                }
            ],
            [
                {
                    "type": "image",
                    "data": {
                        "file": {
                            "url": f"https://{settings.CDN_BASE_DOMAIN}/{'img' * 50}.jpg",
                        },
                        "caption": "Image caption",
                        "withBorder": True,
                        "stretched": False,
                        "withBackground": True,
                    },
                }
            ],
        ),
    )
    def test_cant_post_comment_with_malformed_content(self, content, authed_api_client):
        result = self._post_comment(
            authed_api_client(self.user), self.post, {"content": content}
        )
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()

    def test_malicious_html_tags_in_comment_content_gets_cleaned(self, authed_api_client):
        content_with_tags = (
            "Test <b>html</b>"
            "<script>alert(1);</script>"
            "<br/>"
            "<h1>h1 tag</h1>"
            "<h2>h2 tag</h2>"
            "<h3>h3 tag</h3>"
            "<h4>h4 tag</h4>"
            "<h5>h5 tag</h5>"
            "<h6>h6 tag</h6>"
            "<a href='https://kapi.bar'>link</a>"
            "<a href='ftp://kapi.bar'>link</a>"
        )
        expected_sanitized_content = (
            "Test <b>html</b>"
            "<br>"
            "h1 tag"
            "h2 tag"
            "h3 tag"
            "h4 tag"
            "h5 tag"
            "h6 tag"
            '<a href="https://kapi.bar" target="_blank" '
            'rel="noopener noreferrer nofollow">link</a>'
            '<a target="_blank" rel="noopener noreferrer nofollow">link</a>'
        )
        result = self._post_comment(
            authed_api_client(self.user),
            self.post,
            {
                "content": {
                    "blocks": [{"type": "paragraph", "data": {"text": content_with_tags}}]
                }
            },
        )
        data = result.data
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        assert data["content"]["blocks"][0]["data"]["text"] == expected_sanitized_content

    def test_not_active_user_cant_post_comment(self, authed_api_client):
        user = UserPublicFactory(is_active=False)
        result = self._post_comment(
            authed_api_client(user), self.post, {"content": self.content}
        )
        assert result.status_code == status.HTTP_401_UNAUTHORIZED, result.content.decode()
        assert self.post.comments.count() == 0

    def test_not_logged_in_user_cant_post_comment(self, anon_api_client):
        result = self._post_comment(
            anon_api_client(), self.post, {"content": self.content}
        )
        assert result.status_code == status.HTTP_401_UNAUTHORIZED, result.content.decode()
        assert self.post.comments.count() == 0

    @pytest.mark.parametrize("parent_field_name", ("parent", "parent_uuid"))
    def test_can_reply_to_comment(self, parent_field_name, authed_api_client):
        root_comment = CommentFactory(post=self.post)
        result = self._post_comment(
            authed_api_client(self.user),
            self.post,
            data={
                "content": self.content,
                parent_field_name: root_comment.uuid,
            },
        )
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()
        root_comment.refresh_from_db()
        assert self.post.comments.count() == 2
        child_comment = first(root_comment.get_children())
        data = result.json()

        assert data["uuid"] == str(child_comment.uuid)
        assert data["parent_uuid"] == str(root_comment.uuid)
        assert data["votes_up_count"] == 0
        assert data["votes_down_count"] == 0
        assert int(data["rating"]) == 0
        assert data["can_edit"] is True
        assert data["is_leaf_node"] is True
        assert data["level"] == 1

    def test_cant_reply_to_comment_and_set_other_post(self, authed_api_client):
        root_comment = CommentFactory()
        result = self._post_comment(
            authed_api_client(self.user),
            self.post,
            data={"content": self.content, "parent_uuid": root_comment.uuid},
        )

        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()

    def test_can_edit_comment(self, authed_api_client):
        comment = CommentFactory(post=self.post)
        result = self._edit_comment(
            authed_api_client(comment.user), comment, data={"content": self.content}
        )

        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        comment.refresh_from_db()
        assert result.data["content"] == self.content
        assert comment.content == self.content

        # Проверка создания версии комментария
        versions = Version.objects.get_for_object(Comment.objects.get(uuid=comment.uuid))
        assert versions.count() == 1
        version = versions.first()
        assert version.revision.user_id == comment.user_id
        assert version.revision.comment == "Комментарий отредактирован"
        assert version.field_dict["content"] == self.content

    def test_cannot_edit_not_own_comment(self, authed_api_client):
        comment = CommentFactory(post=self.post)
        result = self._edit_comment(
            authed_api_client(self.user), comment, data={"content": self.content}
        )

        assert result.status_code == status.HTTP_403_FORBIDDEN, result.content.decode()

    def test_not_logged_in_cannot_edit_comment(self, anon_api_client):
        result = self._edit_comment(
            anon_api_client(), CommentFactory(), data={"content": self.content}
        )
        assert result.status_code == status.HTTP_401_UNAUTHORIZED, result.content.decode()

    def test_cannot_edit_comment_with_ratings(self, authed_api_client):
        comment = CommentFactory(post=self.post)
        CommentVoteFactory(comment=comment)
        result = self._edit_comment(
            authed_api_client(comment.user), comment, data={"content": self.content}
        )

        assert result.status_code in [
            status.HTTP_403_FORBIDDEN,
            status.HTTP_400_BAD_REQUEST,
        ], result.content.decode()

    def test_cannot_edit_comment_after_specific_time(self, authed_api_client):
        comment = CommentFactory(post=self.post)
        with freeze_time(
            datetime.timedelta(minutes=settings.COMMENTS_EDITABLE_WINDOW_MINUTES + 1)
        ):
            result = self._edit_comment(
                authed_api_client(comment.user),
                comment,
                data={"content": self.content},
            )

        assert result.status_code == status.HTTP_403_FORBIDDEN, result.content.decode()

    def test_user_can_delete_comment_without_votes_and_child_comment(
        self, authed_api_client
    ):
        comment = CommentFactory(user=self.user)
        result = self._delete_comment(
            authed_api_client(self.user),
            comment,
        )
        assert result.status_code == status.HTTP_204_NO_CONTENT, result.content.decode()
        assert Comment.objects.filter(user=self.user).count() == 0

    def test_user_cannot_delete_comment_with_votes(self, authed_api_client):
        comment = CommentFactory(user=self.user)
        CommentVoteFactory(comment=comment)
        result = self._delete_comment(
            authed_api_client(self.user),
            comment,
        )
        assert result.status_code == status.HTTP_403_FORBIDDEN, result.content.decode()

    def test_user_cannot_delete_comment_with_child_comment(self, authed_api_client):
        comment = CommentFactory(user=self.user)
        CommentFactory(parent=comment)
        result = self._delete_comment(
            authed_api_client(self.user),
            comment,
        )
        assert result.status_code == status.HTTP_403_FORBIDDEN, result.content.decode()

    def test_user_cannot_delete_comment_after_specific_time(self, authed_api_client):
        comment = CommentFactory()
        with freeze_time(
            datetime.timedelta(minutes=settings.COMMENTS_EDITABLE_WINDOW_MINUTES + 1)
        ):
            result = self._delete_comment(
                authed_api_client(comment.user),
                comment,
            )

        assert result.status_code == status.HTTP_403_FORBIDDEN, result.content.decode()

    def test_anon_user_cannot_delete_comment(self, anon_api_client):
        result = self._delete_comment(anon_api_client(), CommentFactory())
        assert result.status_code == status.HTTP_401_UNAUTHORIZED, result.content.decode()

    @pytest.mark.parametrize("logged_in", (True, False))
    def test_cant_fetch_comments_without_post_uuid(
        self, logged_in, authed_api_client, anon_api_client
    ):
        client = authed_api_client(self.user) if logged_in else anon_api_client()
        CommentFactory(post=self.post)
        result = self._get_comments(client)
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert len(result.data) == 0

    @pytest.mark.parametrize("logged_in", (True, False))
    def test_can_fetch_children_comments_for_specific_comment(
        self, logged_in, authed_api_client, anon_api_client
    ):
        root_comment = CommentFactory(post=self.post)
        child_comment = CommentFactory(parent=root_comment, post=self.post)
        client = authed_api_client(self.user) if logged_in else anon_api_client()

        result = self._get_comments(
            client,
            post=self.post.slug,
            parent=root_comment.uuid,
        )

        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        data = result.data
        comment_from_api = first(data)

        assert comment_from_api["uuid"] == str(child_comment.uuid)

    @pytest.mark.parametrize("logged_in", (True, False))
    def test_no_comments_if_parent_comment_not_found(
        self, logged_in, authed_api_client, anon_api_client
    ):
        CommentFactory(parent=CommentFactory(post=self.post), post=self.post)
        client = authed_api_client(self.user) if logged_in else anon_api_client()
        result = self._get_comments(client, post=self.post.slug, parent=uuid.uuid4())
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert len(result.data) == 0
