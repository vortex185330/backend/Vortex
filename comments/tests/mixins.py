from urllib.parse import urlencode

from rest_framework.reverse import reverse


class CommentApiActionsMixin:
    def _get_comments(self, client, **kwargs):
        url = reverse("v1:comments:comments-list")
        if kwargs:
            url += f"?{urlencode(kwargs)}"
        return client.get(url)

    def _post_comment(self, client, post, data):
        data["post"] = post.slug
        return client.post(reverse("v1:comments:comments-list"), data=data)

    def _edit_comment(self, client, comment, data):
        return client.patch(
            reverse("v1:comments:comments-detail", kwargs={"uuid": comment.uuid}),
            data=data,
        )

    def _delete_comment(self, client, comment):
        return client.delete(
            reverse("v1:comments:comments-detail", kwargs={"uuid": comment.uuid}),
        )
