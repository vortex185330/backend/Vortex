from django.db.models import Manager
from django.utils import timezone

from posts.choices import PostStatus
from posts.querysets import PostQuerySet


class PostsManager(Manager.from_queryset(PostQuerySet)):
    ...


class PublishedPostsManager(PostsManager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(
                status=PostStatus.PUBLISHED,
                published_at__lte=timezone.now(),
            )
            .prefetch_related("tags")
            .select_related("user")
        )
