from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.db.models.functions import Lower
from django.utils import timezone
from django.utils.functional import cached_property

from common.helpers import is_prod
from common.models import Timestamped
from posts.choices import PostStatus
from users.choices import UserGender
from users.managers import KapibaraUserManager


class UserPublic(AbstractBaseUser, Timestamped, PermissionsMixin):
    """Information about user."""

    external_user_uid = models.CharField(max_length=36, unique=True, db_index=True)
    username = models.CharField(max_length=100, unique=True)
    email = models.EmailField()
    date_of_birth = models.DateField(null=True, blank=True)
    gender = models.CharField(
        max_length=1, choices=UserGender.choices, default=UserGender.NOT_SPECIFIED
    )
    avatar = models.URLField(blank=True, default="")
    bio = models.TextField(blank=True)
    # For some reason function `get_user_rating` is mostly user instead this field
    rating = models.FloatField(default=0)
    comments_count = models.PositiveIntegerField(default=0)
    votes_up_count = models.PositiveIntegerField(default=0)
    votes_down_count = models.PositiveIntegerField(default=0)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_verified = models.BooleanField(default=False)
    is_banned = models.BooleanField(default=False)
    context = models.OneToOneField(
        "users.UserContext",
        null=True,
        default=None,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="user",
    )
    show_nsfw = models.BooleanField(default=False)
    show_hardcore = models.BooleanField(default=False)
    show_politics = models.BooleanField(default=False)
    show_viewed = models.BooleanField(default=True)
    vote_left_side = models.BooleanField(default=True)
    tags_up_side = models.BooleanField(default=True)
    expand_posts = models.BooleanField(default=False)
    # deep_tree_comment = models.PositiveIntegerField(
    #     default=settings.COMMENTS_TREE_DEFAULT_LEVEL
    # )
    is_official = models.BooleanField(default=False)
    is_from_pikabu = models.BooleanField(default=False)
    last_achievement_check = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name="Последняя проверка достижений",
        help_text="Время последнего запуска проверки достижений для этого пользователя.",
    )

    objects = KapibaraUserManager()

    USERNAME_FIELD = "username"

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"
        constraints = [
            models.UniqueConstraint(
                Lower("username"), name="unique_username_case_insensitive"
            ),
            models.UniqueConstraint(Lower("email"), name="unique_email_case_insensitive"),
        ]

    def __str__(self):
        return self.username

    @cached_property
    def age(self) -> int:
        if not self.date_of_birth:
            return 0
        return (timezone.now().date() - self.date_of_birth).days / 365.25

    def save(self, *args, **kwargs):
        """Save user and set password if passwort doesn't exist on non prod env."""
        if not self.password and not is_prod():
            self.password = make_password(settings.LOADTEST_PASSWORD)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return f"{settings.FRONTEND_BASE_DOMAIN}/profile/{self.username}"

    def get_post_count(self):
        """Возвращает количество опубликованных постов пользователя."""
        return self.posts.filter(status=PostStatus.PUBLISHED).count()

    def get_comment_count(self):
        """Возвращает количество комментариев пользователя."""
        return self.comments.count()

    def get_subscription_count(self):
        """Возвращает количество пользователей, которые на него подписались."""
        return self.subscribers.count()


class UserNote(Timestamped):
    """Model to keep user notes about particular user."""

    author = models.ForeignKey(
        "users.UserPublic", on_delete=models.CASCADE, related_name="written_notes"
    )
    user_about = models.ForeignKey(
        "users.UserPublic", on_delete=models.CASCADE, related_name="notes_about"
    )
    note = models.TextField()

    class Meta:
        unique_together = ("author", "user_about")


class UserContext(models.Model):
    replies_feed_viewed_at = models.DateTimeField(
        null=True,
        default=None,
        blank=True,
        help_text="Время, когда пользователь последний раз просматривал ленту ответов",
    )
    subscriptions_feed_viewed_at = models.DateTimeField(
        null=True,
        default=None,
        blank=True,
        help_text="Время, когда пользователь последний раз просматривал ленту подписок",
    )
    notify_viewed_at = models.DateTimeField(
        null=True,
        default=None,
        blank=True,
        help_text="Время, когда пользователь последний раз просматривал уведомления",
    )
