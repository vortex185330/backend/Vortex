from rest_framework import serializers

from lists.models import TagIgnored, TagSubscription, UserIgnored, UserSubscription
from tags.models import Tag
from users.api.serializers import UserPublicMinimalSerializer
from users.models import UserPublic


class UserSubscriptionSerializer(serializers.ModelSerializer):
    user = UserPublicMinimalSerializer()

    class Meta:
        model = UserSubscription
        fields = (
            "uuid",
            "user",
        )


class CreateUserSubscriptionSerializer(serializers.ModelSerializer):
    subscriber = serializers.HiddenField(default=serializers.CurrentUserDefault())
    username = serializers.SlugRelatedField(
        source="user",
        slug_field="username__iexact",
        write_only=True,
        queryset=UserPublic.objects.all(),
    )
    user = UserPublicMinimalSerializer(read_only=True)

    class Meta:
        model = UserSubscription
        fields = (
            "uuid",
            "subscriber",
            "username",
            "user",
        )

    def validate(self, attrs):
        if attrs["user"] == attrs["subscriber"]:
            raise serializers.ValidationError(
                [
                    {
                        "msg": "Нельзя подписаться на свой аккаунт",
                        "type": "cannot_subscribe_to_themselves",
                    }
                ]
            )
        return attrs

    def create(self, validated_data):
        instance, _ = UserSubscription.objects.get_or_create(**validated_data)
        return instance


class TagSubscriptionSerializer(serializers.ModelSerializer):
    tag = serializers.SlugRelatedField(slug_field="name", queryset=Tag.objects.all())

    class Meta:
        model = TagSubscription
        fields = (
            "uuid",
            "tag",
        )


class CreateTagSubscriptionSerializer(serializers.ModelSerializer):
    subscriber = serializers.HiddenField(default=serializers.CurrentUserDefault())
    tag = serializers.SlugRelatedField(
        slug_field="name",
        queryset=Tag.objects.all(),
    )

    class Meta:
        model = TagSubscription
        fields = (
            "uuid",
            "subscriber",
            "tag",
        )

    def create(self, validated_data):
        instance, _ = TagSubscription.objects.get_or_create(**validated_data)
        return instance


class UserIgnoreSerializer(serializers.ModelSerializer):
    user = UserPublicMinimalSerializer()

    class Meta:
        model = UserIgnored
        fields = (
            "uuid",
            "user",
        )


class CreateUserIgnoreSerializer(serializers.ModelSerializer):
    ignorer = serializers.HiddenField(default=serializers.CurrentUserDefault())
    username = serializers.SlugRelatedField(
        source="user",
        slug_field="username__iexact",
        write_only=True,
        queryset=UserPublic.objects.all(),
    )
    user = UserPublicMinimalSerializer(read_only=True)

    class Meta:
        model = UserIgnored
        fields = (
            "uuid",
            "ignorer",
            "username",
            "user",
        )

    def validate(self, attrs):
        if attrs["user"] == attrs["ignorer"]:
            raise serializers.ValidationError(
                [
                    {
                        "msg": "Нельзя добавить в игнор свой аккаунт",
                        "type": "cannot_ignore_themselves",
                    }
                ]
            )
        return attrs

    def create(self, validated_data):
        instance, _ = UserIgnored.objects.get_or_create(**validated_data)
        return instance


class TagIgnoreSerializer(serializers.ModelSerializer):
    tag = serializers.SlugRelatedField(slug_field="name", queryset=Tag.objects.all())

    class Meta:
        model = TagIgnored
        fields = (
            "uuid",
            "tag",
        )


class CreateTagIgnoreSerializer(serializers.ModelSerializer):
    ignorer = serializers.HiddenField(default=serializers.CurrentUserDefault())
    tag = serializers.SlugRelatedField(
        slug_field="name",
        queryset=Tag.objects.all(),
    )

    class Meta:
        model = TagIgnored
        fields = (
            "uuid",
            "ignorer",
            "tag",
        )

    def create(self, validated_data):
        instance, _ = TagIgnored.objects.get_or_create(**validated_data)
        return instance
