from common.api.pagination import KapibaraCursorPagination


class KapibaraCommentsCursorPagination(KapibaraCursorPagination):
    ordering = "-created_at"
