from datetime import timedelta

from django.http import Http404
from django.utils.timezone import now
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from achievements.api.serializers import AchievementSerializer, UserAchievementSerializer
from achievements.api.v1.policies import AchievementAccessPolicy
from achievements.models import Achievement, UserAchievement
from users.models import UserPublic


class UserAchievementViewSet(ViewSet):
    """ViewSet для управления достижениями пользователя."""

    access_policy = AchievementAccessPolicy

    def list(self, request):
        """Список достижений текущего пользователя."""
        user_achievements = UserAchievement.objects.filter(
            user=request.user, is_rejected=False
        )
        serializer = UserAchievementSerializer(user_achievements, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=["GET"], url_path="by-user/(?P<username>[^/.]+)")
    def achievements_by_user(self, request, username=None):
        """Список достижений для указанного пользователя."""
        user = UserPublic.objects.filter(username=username).first()
        if not user:
            raise Http404({"detail": "Пользователь не найден."})
        user_achievements = UserAchievement.objects.filter(user=user, is_rejected=False)
        serializer = UserAchievementSerializer(user_achievements, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=["POST"], url_path="select-prefix")
    def select_prefix(self, request):
        """Выбрать префикс из достижений."""
        achievement_id = request.data.get("achievement_id")
        if not achievement_id:
            return Response({"detail": "Необходимо указать ID достижения."}, status=400)

        achievement = UserAchievement.objects.filter(
            user=request.user, achievement_id=achievement_id
        ).first()
        if not achievement:
            return Response({"detail": "Достижение не найдено."}, status=404)

        # Сбросить текущий выбранный префикс
        UserAchievement.objects.filter(user=request.user, is_selected_prefix=True).update(
            is_selected_prefix=False
        )

        # Установить новый выбранный префикс
        achievement.is_selected_prefix = True
        achievement.save()

        return Response({"detail": "Префикс успешно установлен."})

    @action(detail=True, methods=["POST"], url_path="reject")
    def reject(self, request, pk=None):
        """Отклонить достижение."""
        achievement = UserAchievement.objects.filter(pk=pk, user=request.user).first()
        if not achievement:
            return Response({"detail": "Достижение не найдено."}, status=404)

        achievement.is_rejected = True
        achievement.save()
        return Response({"detail": "Достижение успешно отклонено."})

    @action(detail=False, methods=["POST"], url_path="check-achievements")
    def check_achievements(self, request):
        """Вызов таски для проверки выполнения условий достижений."""
        user = request.user
        cooldown_period = timedelta(hours=1)  # Разрешаем запускать раз в час

        # Проверяем, можно ли запускать таску
        if (
            user.last_achievement_check
            and now() - user.last_achievement_check < cooldown_period
        ):
            remaining_time = cooldown_period - (now() - user.last_achievement_check)
            minutes_remaining = int(remaining_time.total_seconds() / 60)
            return Response(
                {
                    "detail": f"Вы уже запускали проверку достижений недавно. "
                    f"Попробуйте снова через {minutes_remaining} минут."
                },
                status=429,
            )

        # Обновляем время последнего запуска
        user.last_achievement_check = now()
        user.save()

        # Логика запуска Celery таски
        # Например: check_user_achievements_task.delay(user.id)

        return Response({"detail": "Проверка достижений запущена."})

    @action(detail=False, methods=["GET"], url_path="available")
    def available_achievements(self, request):
        """Список доступных достижений для текущего пользователя."""
        user = request.user

        # Достижения, которые пользователь уже получил
        completed_achievements = UserAchievement.objects.filter(user=user).values_list(
            "achievement_id", flat=True
        )

        # Доступные достижения
        available_achievements = Achievement.objects.exclude(
            id__in=completed_achievements
        )
        serializer = AchievementSerializer(available_achievements, many=True)
        return Response(serializer.data)
