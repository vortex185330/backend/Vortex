import django_filters

from notification.constants import TYPE_CHOICES, NotificationCategories
from notification.models import UserNotification


class UserNotificationFilter(django_filters.FilterSet):
    category = django_filters.ChoiceFilter(choices=NotificationCategories.choices)
    notification_type = django_filters.ChoiceFilter(choices=TYPE_CHOICES)

    class Meta:
        model = UserNotification
        fields = ["category", "notification_type"]
