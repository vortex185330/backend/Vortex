from django.conf import settings
from django.db.models import Count, Q, QuerySet

from posts.choices import PostStatus
from tags.models import Tag
from users.models import UserPublic


def get_tags_for_autosuggestion(
    user: UserPublic, search: str, limit: int = settings.MAX_TAGS_TO_AUTOSUGGEST
) -> QuerySet[Tag]:
    tags = (
        Tag.objects.filter(name__istartswith=search)
        .filter(Q(category__isnull=True) | Q(category__allowed_to_add_manually=True))
        .order_by("-usages")
    )
    if tags.count() < limit:
        other_tags = (
            Tag.objects.filter(name__icontains=search)
            .filter(Q(category__isnull=True) | Q(category__allowed_to_add_manually=True))
            .order_by("-usages")
        )

        tags = tags | other_tags

    return tags.order_by("-usages")[:limit]


def get_popular_used_tags(user: UserPublic, limit: int = settings.MAX_POPULAR_TAGS):
    tags = (
        Tag.objects.filter(post__isnull=False, post__status=PostStatus.PUBLISHED)
        .alias(_count=Count("post"))
        .distinct()
        .order_by("-_count")
    )
    return tags[:limit]
