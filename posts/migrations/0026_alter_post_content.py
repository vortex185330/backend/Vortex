# Generated by Django 4.2.7 on 2023-11-17 18:33

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("posts", "0025_alter_post_status"),
    ]

    operations = [
        migrations.AlterField(
            model_name="post",
            name="content",
            field=models.JSONField(default=dict),
        ),
    ]
