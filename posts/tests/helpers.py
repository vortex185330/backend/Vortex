from datetime import date


def calculate_birth_date(age: int) -> date:
    """
    Calculate the birth date for a given age.

    :param age: Age in years.
    :return: A date object representing the birth date.
    """
    today = date.today()
    birth_year = today.year - age
    birth_date = date(birth_year, today.month, today.day)
    if birth_date > today:
        birth_date = birth_date.replace(year=birth_date.year - 1)
    return birth_date
