from typing import TYPE_CHECKING
from urllib.parse import urlencode

from rest_framework.reverse import reverse

if TYPE_CHECKING:
    from rest_framework.response import Response
    from rest_framework.test import APIClient

    from posts.choices import PostVoteChoice
    from posts.models import Post


class PostsApiActionsMixin:
    def _fetch_posts_feed(self, client, query_params: dict | None = None):
        url = reverse("v1:posts:posts-list")
        if query_params:
            query = urlencode(query_params)
            url = f"{url}?{query}"

        return client.get(url)

    def _fetch_my_posts(self, client, username=None):
        return client.get(reverse("v1:posts:posts-list") + f"?username={username}")

    def _create_post(self, client, data):
        return client.post(reverse("v1:posts:posts-list"), data=data)

    def _delete_post(self, client, post):
        return client.delete(
            reverse("v1:posts:posts-detail", kwargs={"slug": post.slug or post.uuid})
        )

    def _edit_post(self, client, post, data):
        return client.patch(
            reverse("v1:posts:posts-detail", kwargs={"slug": str(post.uuid)}), data=data
        )

    def _get_post(self, client, post):
        return client.get(
            reverse("v1:posts:posts-detail", kwargs={"slug": post.slug or str(post.uuid)})
        )

    def _publish_post(self, client, post):
        return client.post(
            reverse(
                "v1:posts:posts-publish",
                kwargs={"slug": str(post.uuid)},
            )
        )

    def _bulk_post_view(self, client, data):
        return client.post(reverse("v1:posts:post-bulk-views-list"), data=data)

    def _vote_for_post(
        self, client: "APIClient", post: "Post", vote_value: "PostVoteChoice"
    ) -> "Response":
        return client.post(
            reverse("v1:posts:posts-vote", kwargs={"slug": post.slug}),
            data={"value": vote_value},
        )
