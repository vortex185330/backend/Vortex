# Generated by Django 4.2.7 on 2023-11-25 19:55

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("uploads", "0003_alter_image_updated_at"),
    ]

    operations = [
        migrations.AlterField(
            model_name="image",
            name="image_type",
            field=models.CharField(
                choices=[("post", "Post"), ("avatar", "Avatar")],
                default="post",
                max_length=6,
            ),
        ),
    ]
