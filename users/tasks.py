import logging

from celery import shared_task
from django.conf import settings
from django.db import transaction

from comments.services import update_author_comments_count
from posts.models import Post
from tags.models import Tag
from users.models import UserPublic
from users.selectors import (
    get_user_rating,
    get_user_votes_down_count,
    get_user_votes_up_count,
)

logger = logging.getLogger(__name__)


@shared_task
@transaction.atomic
def sync_bayan_vote_to_post(post_id: int):
    post = Post.objects.select_for_update().get(pk=post_id)
    with transaction.atomic():
        bayan_votes_count = post.bayan_votes.count()
        # Если накопилось 5 голосов, добавляем тег "Баян"
        if bayan_votes_count == settings.COUNT_VOTES_FOR_BAYAN:
            bayan_tag = Tag.objects.filter(name="Баян").first()
            if bayan_tag:
                post.tags.add(bayan_tag)
                post.save()


@shared_task
@transaction.atomic
def sync_user_rating_task(user_id: int):
    user = UserPublic.objects.select_for_update().get(pk=user_id)
    user.rating = get_user_rating(user)
    user.save(update_fields=["rating"])


@shared_task
@transaction.atomic
def sync_user_votes_count_task(user_id: int):
    user = UserPublic.objects.select_for_update().get(pk=user_id)
    user.votes_up_count = get_user_votes_up_count(user)
    user.votes_down_count = get_user_votes_down_count(user)
    user.save(update_fields=["votes_up_count", "votes_down_count"])


@shared_task
def sync_users_votes_count_comments_count_and_rating_cronjob():
    for user in UserPublic.objects.iterator():
        logger.info("Syncing user %s", user)
        sync_user_rating_task(user.pk)
        sync_user_votes_count_task(user.pk)
        update_author_comments_count(user.pk)
