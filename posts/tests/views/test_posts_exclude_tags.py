import pytest
from django.conf import settings
from django.test import override_settings
from funcy import lpluck
from rest_framework import status

from posts.choices import PostFeedType, PostStatus
from posts.selectors import get_restricted_tag_names_for_unautorized_and_less_18
from posts.tests.factories import PostFactory
from posts.tests.helpers import calculate_birth_date
from posts.tests.mixins import PostsApiActionsMixin
from tags.tests.factories import TagFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestFilterByTagExclude(PostsApiActionsMixin):
    def setup_method(self):
        self.old_restricted_tag_names = (
            get_restricted_tag_names_for_unautorized_and_less_18()
        )
        self.excluded_tag = TagFactory(name="excluded")

        settings.RESTRICTED_TAG_NAMES_FOR_UNAUTORIZED_AND_LESS_18 = self.excluded_tag.name
        self.included_tag = TagFactory(name="included")
        self.excluded_post = PostFactory(
            status=PostStatus.PUBLISHED, tags=[self.excluded_tag]
        )
        self.included_post = PostFactory(
            status=PostStatus.PUBLISHED, tags=[self.included_tag]
        )

    def teardown_method(self):
        settings.RESTRICTED_TAG_NAMES_FOR_UNAUTORIZED_AND_LESS_18 = (
            self.old_restricted_tag_names
        )

    @override_settings(RESTRICTED_TAG_NAMES_FOR_UNAUTORIZED_AND_LESS_18=["excluded"])
    @pytest.mark.parametrize(
        "feed_type",
        [
            PostFeedType.VIEWED,
            PostFeedType.TRENDS,
            PostFeedType.RATING,
            PostFeedType.SUBSCRIPTIONS,
        ],
    )
    @pytest.mark.parametrize("tag_exclude_value", ["included", "excluded", None])
    def test_excludes_posts_with_specified_tags(
        self, feed_type, tag_exclude_value, anon_api_client
    ):
        result = self._fetch_posts_feed(
            anon_api_client(), {"tag_exclude": tag_exclude_value}
        )
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]

        assert self.excluded_post.slug not in [post["slug"] for post in posts]

    @override_settings(RESTRICTED_TAG_NAMES_FOR_UNAUTORIZED_AND_LESS_18=["excluded"])
    def test_handles_multiple_excluded_tags(self, anon_api_client):
        another_excluded_tag = TagFactory(name="another-excluded")
        another_excluded_post = PostFactory(
            status=PostStatus.PUBLISHED, tags=[another_excluded_tag, self.excluded_tag]
        )
        result = self._fetch_posts_feed(
            anon_api_client(),
            {"tag_exclude": f"{self.excluded_tag.name},{another_excluded_tag.name}"},
        )
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]

        post_slugs = lpluck("slug", posts)

        assert self.excluded_post.slug not in post_slugs
        assert another_excluded_post.slug not in post_slugs
        assert self.included_post.slug in post_slugs

    @override_settings(RESTRICTED_TAG_NAMES_FOR_UNAUTORIZED_AND_LESS_18=["excluded"])
    @pytest.mark.parametrize(
        "feed_type",
        [
            PostFeedType.VIEWED,
            PostFeedType.TRENDS,
            PostFeedType.RATING,
            PostFeedType.SUBSCRIPTIONS,
        ],
    )
    @pytest.mark.parametrize(
        "user_age,expected_exist_exclude_post,tag_exclude_value",
        [
            (17, False, "included"),
            (17, False, "excluded"),
            (17, False, None),
            (18, True, "included"),
            (18, False, "excluded"),
            (18, True, None),
            (19, True, "included"),
            (19, False, "excluded"),
            (19, True, None),
        ],
    )
    def test_filter_age_restrictions(
        self,
        feed_type,
        user_age,
        expected_exist_exclude_post,
        tag_exclude_value,
        authed_api_client,
    ):
        user = UserPublicFactory()
        user.date_of_birth = calculate_birth_date(user_age)
        user.save()

        result = self._fetch_posts_feed(
            authed_api_client(user), {"tag_exclude": tag_exclude_value}
        )
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]

        excluded_post_if_feed = self.excluded_post.slug in lpluck("slug", posts)

        assert excluded_post_if_feed is expected_exist_exclude_post
