import pytest
from funcy import lpluck, lpluck_attr
from rest_framework import status

from lists.models import UserSubscription
from lists.tests.factories import UserSubscriptionFactory
from lists.tests.mixins import ListsApiActionsMixin
from posts.choices import PostFeedType, PostStatus
from posts.tests.factories import PostFactory
from posts.tests.mixins import PostsApiActionsMixin
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestUserSubscriptions(ListsApiActionsMixin, PostsApiActionsMixin):
    def setup_method(self):
        self.subscriber, self.user_to_subscribe = UserPublicFactory.create_batch(size=2)

    def test_user_subscribe_to_another_user(self, authed_api_client):
        response = self._create_user_subscription(
            authed_api_client(self.subscriber), user=self.user_to_subscribe
        )
        assert response.status_code == status.HTTP_201_CREATED, response.content.decode()
        result = response.json()

        db_subscriptions = UserSubscription.objects.filter(
            subscriber=self.subscriber, user=self.user_to_subscribe
        )
        db_subscription = db_subscriptions.first()
        assert db_subscriptions.count() == 1

        assert result["uuid"] == str(db_subscription.uuid)
        assert result["user"]["username"] == self.user_to_subscribe.username
        assert result["user"]["avatar"] == self.user_to_subscribe.avatar

    def test_user_subscribe_to_the_same_user(self, authed_api_client):
        db_subscription = UserSubscriptionFactory(
            subscriber=self.subscriber, user=self.user_to_subscribe
        )
        response = self._create_user_subscription(
            authed_api_client(self.subscriber), user=self.user_to_subscribe
        )
        assert response.status_code == status.HTTP_201_CREATED, response.content.decode()
        result = response.json()

        assert result["uuid"] == str(db_subscription.uuid)
        assert result["user"]["username"] == self.user_to_subscribe.username
        assert result["user"]["avatar"] == self.user_to_subscribe.avatar

    def test_user_cannot_subscribe_to_themselves(self, authed_api_client):
        response = self._create_user_subscription(
            authed_api_client(self.subscriber), user=self.subscriber
        )
        assert (
            response.status_code == status.HTTP_400_BAD_REQUEST
        ), response.content.decode()
        result = response.json()
        assert result["detail"][0]["msg"] == "Нельзя подписаться на свой аккаунт"
        assert result["detail"][0]["type"] == "cannot_subscribe_to_themselves"

    def test_user_cannot_subscribe_to_non_existing_user(self, authed_api_client):
        response = self._create_user_subscription(
            authed_api_client(self.subscriber), user=UserPublicFactory.build()
        )
        assert (
            response.status_code == status.HTTP_400_BAD_REQUEST
        ), response.content.decode()

    def test_anon_user_cannot_subscribe(self, anon_api_client):
        response = self._create_user_subscription(
            anon_api_client(), user=self.user_to_subscribe
        )
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
        ), response.content.decode()

    def test_user_can_list_own_user_subscriptions(self, authed_api_client):
        subscriptions = UserSubscriptionFactory.create_batch(
            size=3, subscriber=self.subscriber
        )
        response = self._get_user_subscriptions(authed_api_client(self.subscriber))
        assert response.status_code == status.HTTP_200_OK, response.content.decode()
        result = response.json()

        sorted_subscriptions = sorted(subscriptions, key=lambda s: s.user.username)
        sorted_subscriptions_from_api = sorted(
            result, key=lambda s: s["user"]["username"]
        )

        assert list(map(str, lpluck_attr("uuid", sorted_subscriptions))) == lpluck(
            "uuid", sorted_subscriptions_from_api
        )

    def test_anon_user_cannot_list_user_subscriptions(self, anon_api_client):
        response = self._get_user_subscriptions(anon_api_client())
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
        ), response.content.decode()

    def test_user_can_unsubscribe_from_user(self, authed_api_client):
        subscription = UserSubscriptionFactory(
            subscriber=self.subscriber, user=self.user_to_subscribe
        )
        response = self._delete_user_subscription(
            authed_api_client(self.subscriber), subscription
        )
        assert (
            response.status_code == status.HTTP_204_NO_CONTENT
        ), response.content.decode()
        assert not UserSubscription.objects.filter(
            subscriber=self.subscriber, user=self.user_to_subscribe
        ).exists()

    def test_user_cant_unsubscribe_from_other_user_subscription(self, authed_api_client):
        subscription = UserSubscriptionFactory(user=self.user_to_subscribe)
        response = self._delete_user_subscription(
            authed_api_client(self.subscriber), subscription
        )
        assert (
            response.status_code == status.HTTP_404_NOT_FOUND
        ), response.content.decode()

    @pytest.mark.parametrize(
        "post_status",
        (
            PostStatus.DRAFT,
            PostStatus.DELETED,
            PostStatus.PUBLISHED,
        ),
    )
    @pytest.mark.parametrize("logged_in", (True, False))
    def test_user_can_see_posts_from_subscribed_users_only(
        self, logged_in, post_status, authed_api_client, anon_api_client
    ):
        subscription = UserSubscriptionFactory(
            subscriber=self.subscriber, user=self.user_to_subscribe
        )
        posts = PostFactory.create_batch(
            size=2, user=subscription.user, status=post_status
        )
        PostFactory.create_batch(size=2, status=PostStatus.PUBLISHED)

        sorted_posts_uuid = list(
            map(
                str,
                lpluck_attr(
                    "uuid", sorted(posts, key=lambda p: p.created_at, reverse=True)
                ),
            )
        )

        response = self._fetch_posts_feed(
            authed_api_client(self.subscriber) if logged_in else anon_api_client(),
            {"feed_type": PostFeedType.SUBSCRIPTIONS},
        )
        result = response.json()
        assert response.status_code == status.HTTP_200_OK, response.content.decode()

        if post_status in (PostStatus.DRAFT, PostStatus.DELETED) or logged_in is False:
            assert len(result["results"]) == 0
            return

        posts_from_api = lpluck("uuid", result["results"])

        assert sorted_posts_uuid == posts_from_api
