from datetime import timedelta

import pytest
from freezegun import freeze_time
from funcy import first, where
from rest_framework import status

from comments.tests.factories import CommentFactory, CommentVoteFactory
from comments.tests.mixins import CommentApiActionsMixin
from posts.choices import PostStatus
from posts.tests.factories import PostFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestModeratorCommentActions(CommentApiActionsMixin):
    def setup_method(self):
        self.moderator = UserPublicFactory(is_staff=True)

    @freeze_time(timedelta(days=-300))
    def test_moderator_can_edit_any_comment(self, authed_api_client):
        comment = CommentFactory()
        new_text = "Edited comment"
        new_content = {"blocks": [{"type": "paragraph", "data": {"text": new_text}}]}
        result = self._edit_comment(
            authed_api_client(self.moderator), comment, {"content": new_content}
        )

        comment.refresh_from_db()
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        assert result.json()["content"]["blocks"][0]["data"]["text"] == new_text
        assert comment.content["blocks"][0]["data"]["text"] == new_text

    @freeze_time(timedelta(days=-300))
    def test_can_edit_flag_on_comment_is_true_for_moderator(self, authed_api_client):
        comment = CommentFactory(post=PostFactory(status=PostStatus.PUBLISHED))
        result = self._get_comments(
            authed_api_client(self.moderator), post=comment.post.slug
        )
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        comment_from_api = first(where(result.json(), uuid=str(comment.uuid)))
        assert comment_from_api["can_edit"] is True

    def test_moderator_can_delete_comment_completely_if_no_votes(self, authed_api_client):
        moderator = UserPublicFactory(is_staff=True)
        result = self._delete_comment(authed_api_client(moderator), CommentFactory())
        assert result.status_code == status.HTTP_204_NO_CONTENT, result.content.decode()

    def test_moderator_cannot_delete_comment_with_votes_completely(
        self, authed_api_client
    ):
        moderator = UserPublicFactory(is_staff=True)
        comment = CommentFactory()
        CommentVoteFactory(comment=comment)
        result = self._delete_comment(authed_api_client(moderator), comment)
        assert result.status_code == status.HTTP_403_FORBIDDEN, result.content.decode()

    def test_moderator_cannot_delete_comment_with_child_comment_completely(
        self, authed_api_client
    ):
        comment = CommentFactory()
        CommentFactory(parent=comment)
        result = self._delete_comment(
            authed_api_client(UserPublicFactory(is_staff=True)), comment
        )
        assert result.status_code == status.HTTP_403_FORBIDDEN, result.content.decode()

    def test_moderator_can_delete_comment_after_specific_time(
        self, authed_api_client, settings
    ):
        comment = CommentFactory()
        with freeze_time(
            timedelta(minutes=settings.COMMENTS_EDITABLE_WINDOW_MINUTES + 1)
        ):
            result = self._delete_comment(
                authed_api_client(UserPublicFactory(is_staff=True)),
                comment,
            )

        assert result.status_code == status.HTTP_204_NO_CONTENT, result.content.decode()
