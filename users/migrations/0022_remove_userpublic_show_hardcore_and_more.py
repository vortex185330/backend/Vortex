# Generated by Django 5.0 on 2024-12-05 15:46

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("users", "0021_usersettings"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="userpublic",
            name="show_hardcore",
        ),
        migrations.RemoveField(
            model_name="userpublic",
            name="show_nsfw",
        ),
        migrations.RemoveField(
            model_name="userpublic",
            name="show_politics",
        ),
        migrations.RemoveField(
            model_name="userpublic",
            name="show_viewed",
        ),
    ]
