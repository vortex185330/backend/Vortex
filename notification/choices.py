from django.db import models


class EmailSendingStatus(models.TextChoices):
    DELIVERED = "delivered", "Доставлено"
    PARTIAL_DELIVERED = "partial delivered", "Доставлено частично"
    PENDING = "pending", "Выслано"
    FAILED = "failed", "Ошибка"
