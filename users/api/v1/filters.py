from typing import TYPE_CHECKING

from django.conf import settings
from django.db.models import Case, IntegerField, Q, When
from django.db.models.functions import Length
from django_filters import rest_framework as filters

from users.models import UserPublic

if TYPE_CHECKING:
    from django.db.models import QuerySet


class UserSearchFilter(filters.FilterSet):
    s = filters.CharFilter(method="filter_by_name")  # noqa: VNE001

    def filter_by_name(
        self, queryset: "QuerySet", name: str, value: str
    ) -> "QuerySet[UserPublic]":
        limit = settings.MAX_USER_TO_SEARCH_AUTOSUGGEST

        # Выполняем один запрос с аннотацией для приоритета и длины никнейма
        users = (
            UserPublic.objects.filter(
                Q(username__istartswith=value) | Q(username__icontains=value)
            )
            .annotate(
                starts_with=Case(
                    When(
                        username__istartswith=value, then=0
                    ),  # приоритет для начала строки
                    default=1,
                    output_field=IntegerField(),
                ),
                username_length=Length(
                    "username"
                ),  # аннотация длины никнейма пользователя
            )
            .order_by("starts_with", "username_length", "-username")[:limit]
        )

        return users
