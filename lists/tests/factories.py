import factory
from factory.django import DjangoModelFactory


class UserSubscriptionFactory(DjangoModelFactory):
    subscriber = factory.SubFactory("users.tests.factories.UserPublicFactory")
    user = factory.SubFactory("users.tests.factories.UserPublicFactory")

    class Meta:
        model = "lists.UserSubscription"


class TagSubscriptionFactory(DjangoModelFactory):
    subscriber = factory.SubFactory("users.tests.factories.UserPublicFactory")
    tag = factory.SubFactory("tags.tests.factories.TagFactory")

    class Meta:
        model = "lists.TagSubscription"


class UserIgnoredFactory(DjangoModelFactory):
    ignorer = factory.SubFactory("users.tests.factories.UserPublicFactory")
    user = factory.SubFactory("users.tests.factories.UserPublicFactory")

    class Meta:
        model = "lists.UserIgnored"


class TagIgnoredFactory(DjangoModelFactory):
    ignorer = factory.SubFactory("users.tests.factories.UserPublicFactory")
    tag = factory.SubFactory("tags.tests.factories.TagFactory")

    class Meta:
        model = "lists.TagIgnored"
