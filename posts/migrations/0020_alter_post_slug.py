# Generated by Django 4.2.6 on 2023-10-26 18:38

from django.db import migrations

import posts.fields


class Migration(migrations.Migration):
    dependencies = [
        ("posts", "0019_alter_post_slug"),
    ]

    operations = [
        migrations.AlterField(
            model_name="post",
            name="slug",
            field=posts.fields.KapibaraPostAutoSlugField(
                blank=True,
                editable=False,
                null=True,
                overwrite=True,
                populate_from=["title"],
            ),
        ),
    ]
