import logging

from django.db.models.query_utils import DeferredAttribute

from users.models import UserContext, UserPublic

logger = logging.getLogger(__name__)


def mark_user_account_verified(user: UserPublic) -> UserPublic:
    """Activate user account."""
    # FIXME: need to add check if user has been banned and raise an error if it has.
    logger.info("Marking user as verified, user %s", user)
    if user.is_verified:
        logger.info("User %s already verified", user)
        return user
    user.is_verified = True
    user.save()
    return user


def mark_user_account_unverified(user: UserPublic) -> UserPublic:
    """Deactivate user account."""
    logger.info("Marking user as unverified, user %s", user)
    if not user.is_verified:
        logger.info("User %s already unverified", user)
        return user
    user.is_verified = False
    user.save()
    return user


def update_user_context_field_with_data(
    user: UserPublic, field_name: DeferredAttribute | str, field_value
) -> UserContext:
    if isinstance(field_name, DeferredAttribute):
        field_name = field_name.field.name

    user_context = user.context

    if not user_context:
        user_context = UserContext.objects.create(**{field_name: field_value})
        user.context = user_context
        user.save(update_fields=["context"])
        return user_context

    if getattr(user_context, field_name) != field_value:
        setattr(user_context, field_name, field_value)
        user_context.save(update_fields=[field_name])

    return user_context
