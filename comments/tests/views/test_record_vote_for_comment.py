import datetime

import pytest
from freezegun import freeze_time
from funcy import first, where
from rest_framework import status
from rest_framework.reverse import reverse

from comments.choices import CommentVoteChoice
from comments.selectors import get_comment_vote_value_for_author
from comments.tests.factories import CommentFactory, CommentVoteFactory
from posts.choices import PostStatus
from posts.tests.factories import PostFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestRecordVoteForComment:
    def setup(self):
        self.comment = CommentFactory(post=PostFactory(status=PostStatus.PUBLISHED))
        self.voter = UserPublicFactory()

    @pytest.mark.parametrize(
        "vote_value,expected_votes_count,expected_comment_rating",
        [
            (CommentVoteChoice.UPVOTE, 1, 1),
            (CommentVoteChoice.DOWNVOTE, 1, -1),
        ],
    )
    def test_voting_first_time(
        self,
        authed_api_client,
        vote_value,
        expected_votes_count,
        expected_comment_rating,
    ):
        client = authed_api_client(self.voter)
        result = self._vote_for_comment(client, self.comment, vote_value)

        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()

        assert result.data == {
            "uuid": str(self.comment.uuid),
            "votes_up_count": 1 if vote_value == CommentVoteChoice.UPVOTE else 0,
            "votes_down_count": 1 if vote_value == CommentVoteChoice.DOWNVOTE else 0,
            "rating": expected_comment_rating,
            "voted": vote_value,
        }

        voter_result = self._get_user(client, self.voter)
        voter_data = voter_result.data
        assert voter_data["votes_up_count"] == (
            1 if vote_value == CommentVoteChoice.UPVOTE else 0
        )
        assert voter_data["votes_down_count"] == (
            1 if vote_value == CommentVoteChoice.DOWNVOTE else 0
        )

        author_result = self._get_user(client, self.comment.user)
        author_result = author_result.data
        assert author_result["rating"] == get_comment_vote_value_for_author(
            self.comment.user, vote_value
        )

    @pytest.mark.parametrize(
        "vote_value", (CommentVoteChoice.UPVOTE, CommentVoteChoice.DOWNVOTE)
    )
    def test_voting_for_own_comment(self, authed_api_client, vote_value):
        client = authed_api_client(self.comment.user)
        result = self._vote_for_comment(client, self.comment, vote_value)
        assert result.status_code == status.HTTP_400_BAD_REQUEST, result.content.decode()

    @pytest.mark.parametrize(
        "vote_value", (CommentVoteChoice.UPVOTE, CommentVoteChoice.DOWNVOTE)
    )
    def test_voting_for_child_comment(self, authed_api_client, vote_value):
        children_comment = CommentFactory(parent=self.comment)
        client = authed_api_client(self.voter)
        result = self._vote_for_comment(client, children_comment, vote_value)
        assert result.status_code == status.HTTP_201_CREATED

    @pytest.mark.parametrize(
        "second_vote", (CommentVoteChoice.UPVOTE, CommentVoteChoice.DOWNVOTE)
    )
    @pytest.mark.parametrize(
        "first_vote", (CommentVoteChoice.UPVOTE, CommentVoteChoice.DOWNVOTE)
    )
    def test_undo_vote(self, authed_api_client, first_vote, second_vote):
        client = authed_api_client(self.voter)
        self._vote_for_comment(client, self.comment, first_vote)
        result = self._vote_for_comment(client, self.comment, second_vote)
        assert result.status_code == status.HTTP_201_CREATED, result.content.decode()

        assert result.data == {
            "uuid": str(self.comment.uuid),
            "votes_up_count": 0,
            "votes_down_count": 0,
            "rating": 0,
            "voted": None,
        }
        voter_result = self._get_user(client, self.voter)
        voter_data = voter_result.data
        assert voter_data["votes_up_count"] == 0
        assert voter_data["votes_down_count"] == 0

        author_result = self._get_user(client, self.comment.user)
        author_result = author_result.data
        assert author_result["rating"] == 0

    def test_cant_vote_as_anonymous(self, anon_api_client):
        result = self._vote_for_comment(
            anon_api_client(), self.comment, CommentVoteChoice.UPVOTE
        )
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    def test_cant_vote_with_wrong_values(self, authed_api_client):
        client = authed_api_client(self.voter)
        result = self._vote_for_comment(client, self.comment, 10)
        assert result.status_code == status.HTTP_400_BAD_REQUEST

    def test_cant_vote_non_active(self, authed_api_client):
        voter = UserPublicFactory(is_active=False)
        client = authed_api_client(voter)
        result = self._vote_for_comment(client, self.comment, CommentVoteChoice.UPVOTE)
        assert result.status_code == status.HTTP_401_UNAUTHORIZED

    @pytest.mark.parametrize(
        "vote", (None, CommentVoteChoice.UPVOTE, CommentVoteChoice.DOWNVOTE)
    )
    def test_vote_choice_shown_for_current_user_in_comments(
        self, vote, authed_api_client, anon_api_client
    ):
        child_comment = CommentFactory(post=self.comment.post, parent=self.comment)
        child_comment_uuid = str(child_comment.uuid)
        if vote:
            CommentVoteFactory(
                comment=child_comment,
                user=self.voter,
                value=-vote if vote is not None else None,
            )
            CommentVoteFactory(comment=self.comment, user=self.voter, value=vote)
        result = self._get_post_comments(authed_api_client(self.voter), self.comment.post)
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        comment = first(where(result.data, uuid=str(self.comment.uuid)))
        child_comment = first(where(result.data, uuid=child_comment_uuid))
        assert comment["voted"] == vote
        assert child_comment["voted"] == (-vote if vote is not None else None)

        # For another user voted should be always None
        another_user = UserPublicFactory()
        result = self._get_post_comments(
            authed_api_client(another_user), self.comment.post
        )
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        comment = first(where(result.data, uuid=str(self.comment.uuid)))
        child_comment = first(where(result.data, uuid=child_comment_uuid))
        assert comment["voted"] is None
        assert child_comment["voted"] is None

        # For anon user voted should be always None
        result = self._get_post_comments(anon_api_client(), self.comment.post)
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        comment = first(where(result.data, uuid=str(self.comment.uuid)))
        child_comment = first(where(result.data, uuid=child_comment_uuid))
        assert comment["voted"] is None
        assert child_comment["voted"] is None

    @pytest.mark.parametrize(
        "is_verified,over_limit,expected_status_code,expected_error_code",
        (
            (
                False,
                True,
                status.HTTP_429_TOO_MANY_REQUESTS,
                "limit_reached_confirm_telegram",
            ),
            (False, False, status.HTTP_201_CREATED, None),
            (True, True, status.HTTP_429_TOO_MANY_REQUESTS, "limit_reached"),
            (True, False, status.HTTP_201_CREATED, None),
        ),
    )
    def test_comment_votes_limit(  # noqa: CFQ002
        self,
        is_verified,
        over_limit,
        expected_status_code,
        expected_error_code,
        authed_api_client,
        settings,
    ):
        settings.LIMIT_VOTES_DEFAULT = 5
        settings.LIMIT_VOTES_FOR_UNVERIFIED = 1
        pre_create_votes = (
            settings.LIMIT_VOTES_DEFAULT
            if is_verified
            else settings.LIMIT_VOTES_FOR_UNVERIFIED
        )
        voter = UserPublicFactory(is_verified=is_verified)
        if over_limit:
            CommentVoteFactory.create_batch(
                size=pre_create_votes, user=voter, value=CommentVoteChoice.UPVOTE
            )

        comment = CommentFactory()

        result = self._vote_for_comment(
            authed_api_client(voter), comment, CommentVoteChoice.UPVOTE
        )

        assert result.status_code == expected_status_code

        if expected_error_code:
            data = result.json()
            assert data["detail"][0]["type"] == expected_error_code

        with freeze_time(
            datetime.datetime.now()
            + datetime.timedelta(hours=settings.LIMIT_TIMEFRAME_HOURS + 1)
        ):
            comment = CommentFactory()
            result = self._vote_for_comment(
                authed_api_client(voter), comment, CommentVoteChoice.UPVOTE
            )
            assert result.status_code == status.HTTP_201_CREATED

    def _vote_for_comment(self, client, comment, vote_value):
        return client.post(
            reverse("v1:comments:comments-vote", kwargs={"uuid": comment.uuid}),
            data={"value": vote_value},
        )

    def _get_post_comments(self, client, post):
        return client.get(reverse("v1:comments:comments-list") + f"?post={post.slug}")

    def _get_user(self, client, user):
        return client.get(
            reverse(
                "v1:users:users-detail",
                kwargs={"username": user.username},
            ),
        )
