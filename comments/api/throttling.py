import re

from rest_framework.throttling import SimpleRateThrottle

duration_re = re.compile(r"(\d+)([smhd])")


class CommentThrottle(SimpleRateThrottle):
    scope_attr = "throttle_scope"

    def __init__(self):
        pass

    def parse_rate(self, rate):
        """
        Given the request rate string, return a two tuple of:
        <allowed number of requests>, <period of time in seconds>
        """
        if rate is None:
            return None, None
        if rate is None:
            return None, None
        num, period = rate.split("/")
        num_requests = int(num)
        duration_multiplier, duration_symbol = duration_re.match(period).groups()

        duration = {"s": 1, "m": 60, "h": 3600, "d": 86400}[duration_symbol] * int(
            duration_multiplier
        )
        return num_requests, duration

    def get_action_rate(self, view):
        try:
            return self.THROTTLE_RATES[self.scope][view.action]
        except KeyError:
            return None

    def allow_request(self, request, view):
        # We can only determine the scope once we're called by the view.
        self.scope = getattr(view, self.scope_attr, None)

        # If a view does not have a `throttle_scope` always allow the request
        if not self.scope:
            return True

        # Determine the allowed request rate as we normally would during
        # the `__init__` call.
        self.rate = self.get_action_rate(view)
        self.num_requests, self.duration = self.parse_rate(self.rate)

        # We can now proceed as normal.
        return super().allow_request(request, view)

    def get_cache_key(self, request, view):
        """
        If `view.throttle_scope` is not set, don't apply this throttle.

        Otherwise generate the unique cache key by concatenating the user id
        with the `.throttle_scope` property of the view.
        """
        if request.user and request.user.is_authenticated:
            ident = request.user.pk
        else:
            ident = self.get_ident(request)

        return self.cache_format % {"scope": self.scope, "ident": ident}
