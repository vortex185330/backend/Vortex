from django.db import models


class UserGender(models.TextChoices):
    FEMALE = "f", "Женский"
    MALE = "m", "Мужской"
    NOT_SPECIFIED = "n", "Не указан"
