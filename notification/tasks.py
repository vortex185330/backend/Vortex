import logging

from celery import shared_task

from comments.models import Comment
from notification.constants import NotificationCategories, NotificationTypes
from notification.models import UserNotification
from posts.models import Post

logger = logging.getLogger(__name__)


class CreateNotificationException(Exception):
    def __init__(self, message="Не удалось создать нотификацию", errors=None):
        super().__init__(message)
        self.errors = errors


@shared_task
def create_user_mention(
    username: str,
    url: str,
    category: NotificationCategories,
    post: Post = None,
    comment: Comment = None,
):
    from users.models import UserPublic

    try:
        if username == "moderator":
            UserNotification.objects.create(
                for_moderators=True,
                message=f"Модератора призвали в {url}",
                notification_type=NotificationTypes.WEB,
                category=NotificationCategories.MODERATION,
                post=post,
                comment=comment,
            )
            return
        user_id = (
            UserPublic.objects.filter(username=username)
            .values_list("id", flat=True)
            .first()
        )
        if user_id:
            UserNotification.objects.create(
                user_id=user_id,
                message=f"@{username}, Вас упомянули в {url}",
                notification_type=NotificationTypes.WEB,
                category=category,
                post=post,
                comment=comment,
            )
    except CreateNotificationException as exc:
        logger.error(f"При создании упоминания " f"пользователя произошла ошибка {exc}")


@shared_task
def create_post_mention(
    slug_post: str,
    url: str,
    category: NotificationCategories,
    post: Post = None,
    comment: Comment = None,
):
    try:
        post_autor_id, username, title = Post.objects.filter(slug=slug_post).values_list(
            "user__id", "user__username", "title"
        ).first() or (None, None, None)
        if post_autor_id:
            UserNotification.objects.create(
                user_id=post_autor_id,
                message=f"@{username}, Ваш пост '{title}' упомянули в {url}",
                notification_type=NotificationTypes.WEB,
                category=category,
                post=post,
                comment=comment,
            )
    except CreateNotificationException as exc:
        logger.error(
            f"При создании упоминания " f"поста пользователя произошла ошибка {exc}"
        )


@shared_task
def create_comment_mention(
    comment_uuid: str,
    url: str,
    category: NotificationCategories,
    post: Post = None,
    comment: Comment = None,
):
    try:
        comment_mentioned = Comment.objects.filter(uuid=comment_uuid).first() or None
        if comment_mentioned:
            UserNotification.objects.create(
                user_id=comment_mentioned.user.id,
                message=f"@{comment_mentioned.user.username}, "
                f"Ваш комментарий {comment_mentioned.get_absolute_url()} "
                f"упомянули в {url}",
                notification_type=NotificationTypes.WEB,
                category=category,
                post=post,
                comment=comment,
            )
    except CreateNotificationException as exc:
        logger.error(
            f"При создании упоминания " f"коммента пользователя произошла ошибка {exc}"
        )
