from django.conf import settings
from storages.backends.s3 import S3ManifestStaticStorage, S3Storage


class StaticFilesStorage(S3ManifestStaticStorage):
    """Storage class for static files."""

    location = "static"
    custom_domain = settings.CDN_BASE_DOMAIN


class UploadedFilesStorage(S3Storage):
    """Storage class for uploaded files."""

    location = "uploads"
    querystring_auth = False
    custom_domain = settings.CDN_BASE_DOMAIN
