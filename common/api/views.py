from django.conf import settings
from django.http import HttpResponse
from django.template.loader import render_to_string
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView, exception_handler
from sentry_sdk import capture_exception, last_event_id

from common.api.helpers import get_meta_data
from common.api.parameters import IS_JSON, PART_PAGE_URL
from common.api.serializers import MetaTagSerializer
from common.constants import SiteMapMaxEntries, SiteMapUrlCategories
from common.models import SitemapEntry


def kapibara_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # If response is not empty, it means it was handled by DRF
    if response is not None:
        return response

    if settings.SENTRY_DSN:
        capture_exception(exc)
        event_id = last_event_id()
    else:
        # If we don't have sentry, then just raise full exception
        return None

    # If response not handler, return server error in JSON
    return Response(
        {
            "detail": [
                {
                    "msg": "Ошибка сервера ☠️",
                    "type": "server_error",
                    "event_id": event_id,
                }
            ]
        },
        status=status.HTTP_500_INTERNAL_SERVER_ERROR,
    )


class GetPreviewPageView(APIView):
    @extend_schema(
        parameters=[PART_PAGE_URL, IS_JSON],
        responses={status.HTTP_200_OK: MetaTagSerializer},
    )
    def get(self, request, *args, **kwargs):
        """Отдает предзаполненную мета-тегами html-страницу перед загрузкой."""
        try:
            meta_data = get_meta_data(request.query_params.get("uri", ""))
        except Exception:
            meta_data = {}

        serializer = MetaTagSerializer(data=meta_data)
        serializer.is_valid(raise_exception=True)
        need_json = request.query_params.get("is_json", True)
        if need_json:
            return Response(serializer.data)
        html_content = render_to_string("preview/index.html", serializer.data)
        return HttpResponse(html_content, content_type="text/html")


def sitemap_index(request):
    entries = {}
    for category in SiteMapUrlCategories:
        count = (
            SitemapEntry.objects.filter(category=category).filter(exist_data=True).count()
        )
        if count > 0:
            entries[category] = count // SiteMapMaxEntries + 1
    response = HttpResponse(content_type="application/xml")
    response.write('<?xml version="1.0" encoding="utf-8"?>\n')
    response.write('<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n')
    for category in entries:
        for index in range(0, entries[category]):
            last_time = (
                SitemapEntry.objects.filter(category=category)
                .filter(exist_data=True)[0]
                .last_visited
            )
            last_time_iso = last_time.isoformat()
            response.write(
                "\t<sitemap>\n"
                f"\t\t<loc>{settings.FRONTEND_BASE_DOMAIN}"
                f"/sitemap/{category}/{index}.xml</loc>\n"
                "\t\t<lastmod>"
                f"{last_time_iso}"
                "</lastmod>\n"
                "\t</sitemap>"
            )
    response.write("</sitemapindex>")
    return response


def sitemap_generator(request, category, index):
    entries = SitemapEntry.objects.filter(category=category).filter(exist_data=True)[
        SiteMapMaxEntries * index : SiteMapMaxEntries * (index + 1)
    ]
    response = HttpResponse(content_type="application/xml")
    response.write('<?xml version="1.0" encoding="utf-8"?>\n')
    response.write('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n')
    for entry in entries:
        if entry.exist_data:
            entry_url = entry.url.replace("&", "&amp;")
            entry_url = entry_url.replace(" ", "+")
            resp = "\t<url>\n" f"\t\t<loc>{entry_url}</loc>\n"
            if category == SiteMapUrlCategories.POST:
                resp += (
                    "\t\t<priority>0.4</priority>\n"
                    "\t\t<changefreq>yearly</changefreq>\n"
                )
            elif category == SiteMapUrlCategories.TAG:
                resp += (
                    "\t\t<priority>0.6</priority>\n"
                    "\t\t<changefreq>hourly</changefreq>\n"
                )
            elif category == SiteMapUrlCategories.USER:
                resp += (
                    "\t\t<priority>0.5</priority>\n"
                    "\t\t<changefreq>daily</changefreq>\n"
                )
            resp += "\t</url>"
            response.write(resp)
    response.write("</urlset>")
    return response
