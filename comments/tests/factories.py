import factory
from factory.django import DjangoModelFactory

from comments.choices import CommentVoteChoice


class CommentFactory(DjangoModelFactory):
    user = factory.SubFactory("users.tests.factories.UserPublicFactory")
    post = factory.SubFactory("posts.tests.factories.PostFactory")
    content = {
        "blocks": [{"type": "paragraph", "data": {"text": "Comment из CommentFactory"}}]
    }

    class Meta:
        model = "comments.Comment"


class CommentVoteFactory(DjangoModelFactory):
    user = factory.SubFactory("users.tests.factories.UserPublicFactory")
    comment = factory.SubFactory(CommentFactory)
    value = CommentVoteChoice.UPVOTE

    class Meta:
        model = "comments.CommentVote"
