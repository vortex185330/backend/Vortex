import pytest
from funcy import first
from rest_framework import status

from posts.choices import PostStatus
from posts.tests.factories import PostFactory
from posts.tests.mixins import PostsApiActionsMixin
from tags.tests.factories import TagFactory


@pytest.mark.django_db
class TestFilterByTag(PostsApiActionsMixin):
    def setup_method(self):
        self.tag_to_search = TagFactory(name="to-search")
        self.post = PostFactory(status=PostStatus.PUBLISHED, tags=[self.tag_to_search])
        PostFactory(status=PostStatus.PUBLISHED, tags=["not-to-search"])

    def test_can_be_filtered_by_tag(self, anon_api_client):
        result = self._fetch_posts_feed(
            anon_api_client(), {"tag": self.tag_to_search.name}
        )
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]
        assert len(posts) == 1
        assert first(posts)["slug"] == self.post.slug

    def test_can_be_filtered_by_multiple_tags_with_and_condition(self, anon_api_client):
        another_tag = TagFactory(name="another-tag")
        post = PostFactory(
            status=PostStatus.PUBLISHED,
            tags=[self.tag_to_search, another_tag, TagFactory()],
        )
        result = self._fetch_posts_feed(
            anon_api_client(),
            {"tag": ",".join([self.tag_to_search.name, another_tag.name])},
        )
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        response = result.json()
        posts = response["results"]
        assert len(posts) == 1
        assert first(posts)["slug"] == post.slug

    @pytest.mark.parametrize("tag,expected_count", (("not-found", 0), ("", 2)))
    def test_result_when_tag_not_found_or_empty(
        self, tag, expected_count, anon_api_client
    ):
        result = self._fetch_posts_feed(anon_api_client(), {"tag": tag})
        assert result.status_code == status.HTTP_200_OK
        response = result.json()
        posts = response["results"]
        assert len(posts) == expected_count
