from unittest.mock import ANY, patch

import pytest
from rest_framework import status
from rest_framework.reverse import reverse


@pytest.mark.django_db
class TestViews:
    def test_return_json_on_500_error(self, anon_api_client, settings):
        settings.SENTRY_DSN = True
        client = anon_api_client()
        with patch("posts.api.v1.views.fetch_posts") as mock_fetch_posts, patch(
            "common.api.views.capture_exception"
        ):
            mock_fetch_posts.side_effect = Exception
            result = client.get(reverse("v1:posts:posts-list"))

        assert (
            result.status_code == status.HTTP_500_INTERNAL_SERVER_ERROR
        ), result.content.decode()
        data = result.json()
        assert data["detail"] == [
            {"event_id": ANY, "msg": "Ошибка сервера ☠️", "type": "server_error"}
        ]

    def test_robots_txt(self, anon_api_client):
        client = anon_api_client()

        result = client.get("/robots.txt")

        assert result.status_code == status.HTTP_200_OK
        assert result.headers["Content-Type"] == "text/plain"
