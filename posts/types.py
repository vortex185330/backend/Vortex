import datetime
import typing


class SinglePostView(typing.TypedDict):
    slug: str
    viewed_at: datetime.datetime


BulkPostViewData = list[SinglePostView]
