from django.contrib import admin

from tags.models import Tag, TagCategory


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "category",
        "usages",
    )
    list_select_related = ("category",)
    ordering = (
        "name",
        "-usages",
    )
    readonly_fields = ("usages",)
    search_fields = (
        "name",
        "category__name",
    )
    list_filter = ("category",)


@admin.register(TagCategory)
class TagCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "send_post_for_moderation",
        "allowed_to_add_manually",
    )
    search_fields = ("name",)

    list_display_links = ("name",)

    ordering = ("name",)

    list_filter = (
        "send_post_for_moderation",
        "allowed_to_add_manually",
    )

    list_editable = (
        "send_post_for_moderation",
        "allowed_to_add_manually",
    )
