# Generated by Django 4.2.6 on 2023-10-18 05:38

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("users", "0009_alter_userpublic_tags_alter_usertag_tag"),
    ]

    operations = [
        migrations.AlterField(
            model_name="userpublic",
            name="avatar",
            field=models.URLField(blank=True, default=""),
        ),
    ]
