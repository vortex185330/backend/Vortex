def is_apng(image_content: bytes) -> bool:
    ac_tl = image_content.find(b"\x61\x63\x54\x4C")
    if ac_tl <= 0:
        return False
    i_dat = image_content.find(b"\x49\x44\x41\x54")
    return ac_tl < i_dat
