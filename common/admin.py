from django.contrib import admin

from .models import SitemapEntry, SitemapTask
from .tasks import update_sitemap


@admin.register(SitemapTask)
class SitemapTaskAdmin(admin.ModelAdmin):
    list_display = (
        "task_uid",
        "status",
        "feeds",
        "profiles",
        "posts",
        "tags",
        "all_time",
        "created_at",
        "finished_at",
    )
    search_fields = ("task_uid",)
    ordering = ("created_at",)
    exclude = ("task_uid",)  # Исключаем поле task_uid из формы для администрирования

    actions = [
        "run_update_sitemap_with_clean",
        "run_update_sitemap_without_clean",
        "clear_sitemap_entries_for_task",
    ]

    def run_update_sitemap_without_clean(self, request, queryset):
        for choiced_sm_task in queryset:
            update_sitemap.delay(choiced_sm_task.task_uid, with_clean=False)
            self.message_user(
                request,
                f"Задача {choiced_sm_task.task_uid} обновления таблицы "
                f"sitemap без очистки  запущена.",
            )

    run_update_sitemap_without_clean.short_description = (
        "Обновить таблицу sitemap без очистки"
    )

    def run_update_sitemap_with_clean(self, request, queryset):
        for choiced_sm_task in queryset:
            update_sitemap.delay(choiced_sm_task.task_uid, with_clean=True)
            self.message_user(
                request,
                f"Задача {choiced_sm_task.task_uid} обновления таблицы "
                f"sitemap с очисткой запущена.",
            )

    run_update_sitemap_with_clean.short_description = (
        "Обновить таблицу sitemap с очисткой"
    )

    def clear_sitemap_entries_for_task(self, request, queryset):
        for choiced_sm_task in queryset:
            SitemapEntry.objects.filter(task_uid=choiced_sm_task.task_uid).all().delete()
            self.message_user(
                request,
                f"Sitemap Entry c task uid {choiced_sm_task.task_uid} удалены",
            )

    clear_sitemap_entries_for_task.short_description = (
        "Удалить SitemapEntry для выбранной задачи"
    )

    # def to_dict(self):
    #     return {
    #         'task_uid': self.task_uid,
    #         'status': self.status,
    #         'feeds': self.status,
    #         'profiles': self.status,
    #         'posts': self.status,
    #         'tags': self.status,
    #         'created_at': self.finished_at,
    #         'finished_at': self.created_at,
    #     }


@admin.register(SitemapEntry)
class SitemapEntryAdmin(admin.ModelAdmin):
    list_display = (
        "url",
        "last_visited",
        "category",
        "get_task_status",
        "task_uid",
        "exist_data",
    )
    search_fields = ("url",)
    list_filter = ("category", "task_uid", "exist_data")
    ordering = ("last_visited",)

    def get_task_status(self, obj):
        task = SitemapTask.objects.get(task_uid=obj.task_uid)
        return task.status if task else "Нет задачи"

    get_task_status.short_description = "Статус задачи"
