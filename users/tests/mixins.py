from typing import TYPE_CHECKING

from rest_framework.reverse import reverse

if TYPE_CHECKING:
    from rest_framework.response import Response
    from rest_framework.test import APIClient


class UserApiActionsMixin:
    def _create_user(self, client, data, headers=None):
        return client.post(reverse("v1:users:users-list"), data=data, headers=headers)

    def _verify_unverify_user(self, action, client, user, headers=None):
        return client.post(
            reverse(
                f"v1:users:users-{action}",
                kwargs={"username": user.username},
            ),
            headers=headers,
        )

    def _update_user(self, client, user, data):
        return client.patch(
            reverse(
                "v1:users:users-detail",
                kwargs={"username": user.username},
            ),
            data=data,
        )

    def _get_user(self, client, user):
        return client.get(
            reverse(
                "v1:users:users-detail",
                kwargs={"username": user.username},
            ),
        )

    def _get_user_context(self, client):
        return client.get(reverse("v1:user-context"))

    def _search_for_user(
        self, client: "APIClient", username: str | None = None
    ) -> "Response":
        query_parameters = {}
        if username:
            query_parameters["s"] = username

        return client.get(reverse("v1:user-search"), query_parameters)
