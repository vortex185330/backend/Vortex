import datetime

import pytest
from django.utils import timezone
from rest_framework import status

from comments.services import update_post_comments_count
from comments.tests.factories import CommentFactory
from posts.choices import PostFeedType, PostStatus, TimePeriod
from posts.tests.factories import PostFactory
from posts.tests.mixins import PostsApiActionsMixin


@pytest.mark.django_db
class TestCommentedFeed(PostsApiActionsMixin):
    def setup_method(self):
        now = timezone.now()
        self.post_0_comments = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=40)
        )
        self.post_2_comments = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=2)
        )
        self.post_1_comment = PostFactory(status=PostStatus.PUBLISHED, published_at=now)
        self.post_3_comments = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=8)
        )
        self.post_4_comments_same_date_as_3 = PostFactory(
            status=PostStatus.PUBLISHED, published_at=now - datetime.timedelta(days=8)
        )
        CommentFactory(post=self.post_1_comment)
        CommentFactory.create_batch(size=2, post=self.post_2_comments)
        CommentFactory.create_batch(size=3, post=self.post_3_comments)
        CommentFactory.create_batch(size=4, post=self.post_4_comments_same_date_as_3)
        for post in (
            self.post_0_comments,
            self.post_1_comment,
            self.post_2_comments,
            self.post_3_comments,
            self.post_4_comments_same_date_as_3,
        ):
            update_post_comments_count(post.pk)

    @pytest.mark.parametrize(
        "period,expected_ordering",
        (
            (
                "",
                ["post_2_comments", "post_1_comment"],
            ),
            (
                TimePeriod.ALL,
                [
                    "post_4_comments_same_date_as_3",
                    "post_3_comments",
                    "post_2_comments",
                    "post_1_comment",
                    "post_0_comments",
                ],
            ),
            (
                TimePeriod.MONTH,
                [
                    "post_4_comments_same_date_as_3",
                    "post_3_comments",
                    "post_2_comments",
                    "post_1_comment",
                ],
            ),
            (TimePeriod.WEEK, ["post_2_comments", "post_1_comment"]),
            (TimePeriod.DAY, ["post_1_comment"]),
        ),
    )
    def test_commented_feed(self, anon_api_client, period, expected_ordering):
        result = self._fetch_posts_feed(
            anon_api_client(), {"period": period, "feed_type": PostFeedType.COMMENTED}
        )
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        data = result.json()
        results = data["results"]
        expected_ordering = [str(getattr(self, post).uuid) for post in expected_ordering]
        ordering_from_api = [post["uuid"] for post in results]
        assert ordering_from_api == expected_ordering
