import factory
from factory.django import DjangoModelFactory


class TagFactory(DjangoModelFactory):
    name = factory.Faker("sentence", nb_words=2)

    class Meta:
        model = "tags.Tag"
        django_get_or_create = ("name",)


class TagCategory(DjangoModelFactory):
    name = factory.Faker("word")

    class Meta:
        model = "tags.TagCategory"
