from rest_framework.routers import SimpleRouter

from achievements.api.v1.views import UserAchievementViewSet

router = SimpleRouter()
router.register("achievements", UserAchievementViewSet, basename="achievements")

urlpatterns = router.urls
