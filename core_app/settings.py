import datetime
from pathlib import Path

import environ

env = environ.Env()

BASE_DIR = Path(__file__).resolve().parent.parent
SECRET_KEY = env.str(
    "SECRET_KEY", "django-insecure-34oqj72*yq6cky9nrubxyaw1^hvyybp&7=+uw%f-6wac%og4pn"
)

CORS_ALLOWED_ORIGINS = env.list(
    "CORS_ALLOWED_ORIGINS",
    str,
    [
        "https://kapi.bar",
        "https://alpha.kapi.bar",
        "https://dev.kapi.bar",
        "http://localhost:5173",
        "http://127.0.0.1:5173",
    ],
)

DEBUG = env.bool("DEBUG", default=False)
ENVIRONMENT = env.str("ENVIRONMENT", default="local")
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", str, ["*"])
CSRF_COOKIE_DOMAIN = env.str("CSRF_COOKIE_DOMAIN", default=None)
CSRF_TRUSTED_ORIGINS = env.list("CSRF_TRUSTED_ORIGINS", cast=str, default=[])
USE_SECURE_PROXY_SSL_HEADER = env.bool("USE_SECURE_PROXY_SSL_HEADER", default=False)
if USE_SECURE_PROXY_SSL_HEADER:
    SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")


DJANGO_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]

THIRD_PARTY_APPS = [
    "cacheops",
    "corsheaders",
    "django_extensions",
    "django_filters",
    "django_otp",
    "django_otp.plugins.otp_static",
    "django_otp.plugins.otp_totp",
    "drf_spectacular",
    "jsoneditor",
    "mptt",
    "rest_framework",
    "reversion",
    "two_factor",
]

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

INSTALLED_APPS = [
    *DJANGO_APPS,
    *THIRD_PARTY_APPS,
    "comments",
    "communities",
    "lists",
    "posts",
    "tags",
    "uploads",
    "users",
    "notification",
    "common",
    "django_celery_beat",
    "achievements",
]

TWO_FACTOR_PATCH_ADMIN = env.bool("ENABLE_DJANGO_ADMIN_OTP", True)

if TWO_FACTOR_PATCH_ADMIN:
    MIDDLEWARE.append("django_otp.middleware.OTPMiddleware")
    INSTALLED_APPS.append("core_app.apps.KapibaraAdminOTPConfig")
    LOGIN_URL = "two_factor:login"
else:
    INSTALLED_APPS.append("core_app.apps.KapibaraAdminConfig")


if env.bool("ENABLE_DB_STATS", default=False):
    MIDDLEWARE += ["common.middlewares.DBStatsMiddleware"]

ROOT_URLCONF = "core_app.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR / "templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "core_app.wsgi.application"

DATABASES = {"default": env.db()}

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",  # noqa: E501
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


LANGUAGE_CODE = "ru-RU"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True


STATIC_URL = "/static/"
STATIC_ROOT = BASE_DIR / "static"


DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"
AUTH_USER_MODEL = "users.UserPublic"

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
        },
    },
    "root": {
        "handlers": ["console"],
        "level": "DEBUG" if DEBUG else "INFO",
    },
    "loggers": {
        "django.db": {
            "handlers": ["console"],
            "level": "DEBUG" if DEBUG else "INFO",
            "propagate": False,
        },
    },
}

REST_FRAMEWORK = {
    "EXCEPTION_HANDLER": "common.api.views.kapibara_exception_handler",
    "NON_FIELD_ERRORS_KEY": "detail",
    "DEFAULT_RENDERER_CLASSES": [
        "rest_framework.renderers.JSONRenderer",
    ],
    "DEFAULT_PARSER_CLASSES": [
        "rest_framework.parsers.JSONParser",
    ],
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework_simplejwt.authentication.JWTAuthentication",
    ),
    "DEFAULT_FILTER_BACKENDS": ["django_filters.rest_framework.DjangoFilterBackend"],
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.PageNumberPagination",
    "PAGE_SIZE": 50,
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
    "DATETIME_FORMAT": "iso-8601",
    "TEST_REQUEST_DEFAULT_FORMAT": "json",
    "DEFAULT_THROTTLE_CLASSES": [
        "rest_framework.throttling.ScopedRateThrottle",
    ],
    "DEFAULT_THROTTLE_RATES": {
        "upload-image": "100/min",
        "tag-search": "30/min",
        "comments": {"create": "1/4s"},
    },
}

SPECTACULAR_SETTINGS = {
    "TITLE": "Kapibara API",
    "DESCRIPTION": "API for Kapibara",
    "VERSION": "0.0.1-alpha",
    "SERVE_INCLUDE_SCHEMA": False,
    "SWAGGER_UI_SETTINGS": {
        "deepLinking": True,
        "persistAuthorization": True,
    },
}

RSA_PRIVATE_KEY = env.str("RSA_PRIVATE_KEY", SECRET_KEY)
RSA_PUBLIC_KEY = env.str("RSA_PUBLIC_KEY", "")
SIMPLE_JWT = {
    "ALGORITHM": "RS512" if RSA_PRIVATE_KEY and RSA_PUBLIC_KEY else "HS256",
    "ACCESS_TOKEN_LIFETIME": datetime.timedelta(
        minutes=env.int("JWT_ACCESS_TOKEN_LIFETIME_MINUTES", 5)
    ),
    "REFRESH_TOKEN_LIFETIME": datetime.timedelta(
        days=env.int("JWT_REFRESH_TOKEN_LIFETIME_DAYS", 1)
    ),
    "SIGNING_KEY": RSA_PRIVATE_KEY,
    "VERIFYING_KEY": RSA_PUBLIC_KEY,
    "USER_ID_FIELD": "external_user_uid",
}

DJANGO_ADMIN_PATH = env.str("DJANGO_ADMIN_PATH", "admin")
COMMENT_VOTE_RATING_COEFF = env.float("COMMENT_VOTE_RATING_COEFF", 0.5)
INTERNAL_TOKEN_HEADER = env.str("INTERNAL_TOKEN_HEADER", "X-Kapibara-Internal-Token")
INTERNAL_TOKENS = env.list("INTERNAL_TOKENS", cast=str, default=[])
LOADTEST_PASSWORD = env.str("LOADTEST_PASSWORD", "testpassword")

# SMTP
SMTP_HOST = env.str("SMTP_HOST", "")
SMTP_PORT = env.int("SMTP_PORT", 587)
SMTP_USERNAME = env.str("SMTP_USERNAME", "")
SMTP_PASSWORD = env.str("SMTP_PASSWORD", "")
DEFAULT_FROM_EMAIL = env.str("DEFAULT_FROM_EMAIL", "no-reply@kapi.bar")

COMMENTS_EDITABLE_WINDOW_MINUTES = env.int("COMMENTS_EDITABLE_WINDOW_MINUTES", 10)
COMMENTS_TREE_DEFAULT_LEVEL = env.int("COMMENTS_TREE_DEFAULT_LEVEL", 2)
COMMENT_RATING_MULTIPLIER = env.float("COMMENT_RATING_MULTIPLIER", 0.5)

POST_RATING_MULTIPLIER = env.float("POST_RATING_MULTIPLIER", 1)
POST_EDITABLE_WINDOW_MINUTES = env.int("POST_EDITABLE_WINDOW_MINUTES", 60 * 24)

POST_MIN_POSITIVE_VOTES_FOR_TRENDS_RATING_FEED = env.int(
    "POST_MIN_POSITIVE_VOTES_FOR_TRENDS_RATING_FEED", 15
)
POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TRENDS_RATING_FEED = env.int(
    "POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TRENDS_RATING_FEED", 70
)
POST_MIN_POSITIVE_VOTES_FOR_TOP_RATING_FEED = env.int(
    "POST_MIN_POSITIVE_VOTES_FOR_TOP_RATING_FEED", 35
)
POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TOP_RATING_FEED = env.int(
    "POST_MIN_POSITIVE_VOTES_PERCENTAGE_FOR_TOP_RATING_FEED", 80
)

MAX_TAGS_TO_AUTOSUGGEST = env.int("MAX_TAGS_TO_AUTOSUGGEST", 20)
MAX_USER_TO_SEARCH_AUTOSUGGEST = env.int("MAX_USER_TO_SEARCH_AUTOSUGGEST", 20)
MAX_POPULAR_TAGS = env.int("MAX_POPULAR_TAGS", 20)
MIN_TAGS_IN_POST = env.int("MIN_TAGS_IN_POST", 2)
MAX_TAGS_IN_POST = env.int("MAX_TAGS_IN_POST", 20)

LIMIT_DRAFT_POSTS = env.int("LIMIT_DRAFT_POSTS", 20)
LIMIT_TIMEFRAME_HOURS = env.int("LIMIT_TIMEFRAME_HOURS", 24)
LIMIT_POSTS_FOR_UNVERIFIED = env.int("LIMIT_POSTS_FOR_UNVERIFIED", 0)
LIMIT_POSTS_DEFAULT = env.int("LIMIT_POSTS_DEFAULT", 20)
LIMIT_COMMENTS_DEFAULT = env.int("LIMIT_COMMENTS_DEFAULT", 1000)
LIMIT_COMMENTS_FOR_UNVERIFIED = env.int("LIMIT_COMMENTS_FOR_UNVERIFIED", 500)
LIMIT_VOTES_DEFAULT = env.int("LIMIT_VOTES_DEFAULT", 1000)
LIMIT_VOTES_FOR_UNVERIFIED = env.int("LIMIT_VOTES_FOR_UNVERIFIED", 1)

RATING_FOR_AUTOBAN = env.float("RATING_FOR_AUTOBAN", -300.0)
MINIMAL_AGE_FOR_NSFW = env.float("MINIMAL_AGE_FOR_NSFW", 18)
MIN_DISPLAYED_POST_RATING = env.float("MIN_DISPLAYED_POST_RATING", -15.0)
RESTRICTED_TAG_NAMES_FOR_UNAUTORIZED_AND_LESS_18 = env.list(
    "RESTRICTED_TAG_NAMES_FOR_UNAUTORIZED_AND_LESS_18",
    default=["NSFW", "Жесть", "Политика", "18+"],
)
NORATING_TAG_NAMES = env.list("NORATING_TAG_NAMES", default=["Без рейтинга", "Баян"])
COUNT_VOTES_FOR_BAYAN = env.int("COUNT_VOTES_FOR_BAYAN", default=5)
MAXIMUM_COUNT_VOTES_FOR_BAYAN_FOR_USER = env.int(
    "MAXIMUM_COUNT_VOTES_FOR_BAYAN_FOR_USER", default=30
)
DEFAULT_MESSAGE_RESTRICTED_CONTENT = env.str(
    "DEFAULT_MESSAGE_RESTRICTED_CONTENT",
    "Это контент 18+. Чтобы увидеть содержимое, "
    '<a href="http://kapi.bar/registration" rel="noopener noreferrer nofollow">'
    "зарегистрируйтесь</a> и верифицируйте профиль или "
    '<a href="http://kapi.bar/settings/main" rel="noopener noreferrer nofollow">'
    f"подтвердите</a>, что Вам есть {MINIMAL_AGE_FOR_NSFW} лет.",
)

# MetaTags config
DEFAULT_TITLE = env.str("DEFAULT_TITLE", "Капибара - развлекательный портал.")
DEFAULT_DESCRIPTION = env.str(
    "DEFAULT_DESCRIPTION",
    "Kapi.Bar - согревательная атмосфера, крафтовый контент и "
    "зажигательные комменты, у нас можно со своим, с нами время "
    "летит не заметно. Kapi.Bar - мы открылись, откройся и ты. ",
)
DEFAULT_KEYWORDS = env.str(
    "DEFAULT_KEYWORDS", "Капибара - развлекательный портал, мемы, юмор, развлечения, арты"
)
DEFAULT_H1 = env.str("DEFAULT_H1", "")
DEFAULT_PATH_IMAGE = env.str(
    "DEFAULT_PATH_IMAGE", "https://kapi.bar/assets/images/Userpic.svg"
)
DEFAULT_MAX_LENGTH_OG_DESCRIPTIONS = env.int("DEFAULT_MAX_LENGTH_OG_DESCRIPTIONS", 155)
DEFAULT_MAX_LENGTH_DESCRIPTIONS = env.int("DEFAULT_MAX_LENGTH_DESCRIPTIONS", 200)
DEFAULT_DESCRIPTION_NEW = env.str("DEFAULT_DESCRIPTION_NEW", "Новые посты на Kapi.Bar")
DEFAULT_DESCRIPTION_TOP = env.str("DEFAULT_DESCRIPTION_TOP", "Лучшие посты на Kapi.Bar")
DEFAULT_DESCRIPTION_DISCUSSED = env.str(
    "DEFAULT_DESCRIPTION_DISCUSSED", "Самые обсуждаемые посты на Kapi.Bar"
)
DEFAULT_DESCRIPTION_COPYRIGHT = env.str(
    "DEFAULT_DESCRIPTION_COPYRIGHT", "Авторские посты на Kapi.Bar"
)
DEFAULT_DESCRIPTION_TAGS = env.str(
    "DEFAULT_DESCRIPTION_TAGS", "Поиск постов по тегам на Kapi.Bar"
)
MONOLITH_URL = env.str(
    "MONOLITH_URL",
    "backend-dev.kapi.bar"
    if (DEBUG or ENVIRONMENT == "dev")
    else "backend.alpha.kapi.bar",
)

# Storages config
if USE_CLOUD_STORAGE := env.bool("USE_CLOUD_STORAGE", False):
    STORAGES = {
        "default": {"BACKEND": "common.storages.UploadedFilesStorage"},
        "staticfiles": {"BACKEND": "common.storages.StaticFilesStorage"},
    }

    AWS_ACCESS_KEY_ID = env.str("AWS_ACCESS_KEY_ID")
    AWS_SECRET_ACCESS_KEY = env.str("AWS_SECRET_ACCESS_KEY")

AWS_STORAGE_BUCKET_NAME = env.str("UPLOADS_STORAGE_BUCKET_NAME", "kapibucket-images-prod")
AWS_DEFAULT_ACL = "public-read"
AWS_S3_REGION_NAME = "fra1"
AWS_S3_OBJECT_PARAMETERS = {"CacheControl": "max-age=86400"}
AWS_S3_ENDPOINT_URL = f"https://{AWS_S3_REGION_NAME}.digitaloceanspaces.com"
CDN_BASE_DOMAIN = env.str(
    "CDN_BASE_DOMAIN", "kapibucket-images-prod.fra1.cdn.digitaloceanspaces.com"
)


if SENTRY_DSN := env.str("MONOLITH_SENTRY_DSN", ""):
    import sentry_sdk
    from sentry_sdk.integrations.celery import CeleryIntegration
    from sentry_sdk.integrations.django import DjangoIntegration
    from sentry_sdk.integrations.redis import RedisIntegration

    sentry_sdk.init(
        dsn=SENTRY_DSN,
        traces_sample_rate=1.0,
        profiles_sample_rate=1.0,
        send_default_pii=True,
        environment=ENVIRONMENT,
        integrations=[
            DjangoIntegration(),
            CeleryIntegration(),
            RedisIntegration(),
        ],
        release="backend@%%VERSION%%",
    )

CELERY_TASK_ALWAYS_EAGER = env.bool("CELERY_TASK_ALWAYS_EAGER", False)
CELERY_BROKER_URL = env.str(
    "CELERY_BROKER_URL", default="redis://kapibara-monolith-redis:6379/2"
)
CELERY_TASK_SERIALIZER = "json"

CELERY_TASK_RETRY_COUNTDOWN = 30
CELERY_EAGER_PROPAGATES = True
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_IGNORE_RESULT = True
CELERY_RESULT_BACKEND = CELERY_BROKER_URL
CELERY_RESULT_EXPIRES = 1
CELERY_TIMEZONE = TIME_ZONE
CELERY_ENABLE_UTC = True

CORS_ALLOW_ALL_ORIGINS = True

ALLOWED_TAGS_IN_HTML_CONTENT = {
    "a",
    "b",
    "em",
    "strong",
    "p",
    "br",
    "img",
    "i",
    "pre",
    "s",
    "ol",
    "ul",
    "li",
    "blockquote",
    "u",
    "text_spoiler",
    "spoiler",
    "details",
    "target",
}

# Максимальный объем текста который может быть опубликован в одном блоке ContentTypeHtml
CONTENT_BLOCK_MAX_LENGTH_BYTES = env.int("CONTENT_BLOCK_MAX_LENGTH_BYTES", 10000)
# Максимальный объем текста для подписи фото / цитаты
CONTENT_CAPTION_MAX_LENGTH_BYTES = env.int("CONTENT_CAPTION_MAX_LENGTH_BYTES", 500)
# Максимальный объем текста для элемента списка
CONTENT_LIST_ITEM_MAX_LENGTH_BYTES = env.int("CONTENT_LIST_ITEM_MAX_LENGTH_BYTES", 1000)
# Максимальное количество элементов в списке
CONTENT_MAX_LIST_ITEMS = env.int("CONTENT_MAX_LIST_ITEMS", 50)
# Максимальное количество блоков которое может быть добавлено в пост
CONTENT_POST_BLOCK_MAX_HTML_ITEMS = env.int("CONTENT_POST_BLOCK_MAX_HTML_ITEMS", 150)
# Максимальное количество блоков изображений которое может быть добавлено в пост
CONTENT_POST_BLOCK_IMG_MAX_ITEMS = env.int("CONTENT_POST_BLOCK_IMG_MAX_ITEMS", 50)
# Максимальное количество блоков видео которое может быть добавлено в пост
CONTENT_POST_BLOCK_VIDEO_MAX_ITEMS = env.int("CONTENT_POST_BLOCK_IMG_MAX_ITEMS", 50)
# Максимальное общее количество блоков в посте, по умолчанию сумма html + img + video
CONTENT_POST_BLOCK_MAX_TOTAL_ITEMS = env.int(
    "CONTENT_POST_BLOCK_MAX_TOTAL_ITEMS",
    CONTENT_POST_BLOCK_MAX_HTML_ITEMS
    + CONTENT_POST_BLOCK_IMG_MAX_ITEMS
    + CONTENT_POST_BLOCK_VIDEO_MAX_ITEMS,
)

# Максимальное количество блоков которое может быть добавлено в коммент
CONTENT_COMMENT_BLOCK_MAX_HTML_ITEMS = env.int("CONTENT_COMMENT_BLOCK_MAX_HTML_ITEMS", 10)
# Максимальное количество блоков изображений которое может быть добавлено в коммент
CONTENT_COMMENT_BLOCK_IMG_MAX_ITEMS = env.int("CONTENT_COMMENT_BLOCK_IMG_MAX_ITEMS", 2)
# Максимальное количество блоков видео которое может быть добавлено в коммент
CONTENT_COMMENT_BLOCK_VIDEO_MAX_ITEMS = env.int("CONTENT_COMMENT_BLOCK_IMG_MAX_ITEMS", 2)
# Максимальное общее количество блоков в комменте, по умолчанию сумма html + img + video
CONTENT_COMMENT_BLOCK_MAX_TOTAL_ITEMS = env.int(
    "CONTENT_COMMENT_BLOCK_MAX_TOTAL_ITEMS",
    CONTENT_COMMENT_BLOCK_MAX_HTML_ITEMS
    + CONTENT_COMMENT_BLOCK_IMG_MAX_ITEMS
    + CONTENT_COMMENT_BLOCK_VIDEO_MAX_ITEMS,
)

# Максимальный объем gif или apng файла для загрузки
IMAGE_MAX_GIF_APNG_SIZE_BYTES = env.int("IMAGE_MAX_GIF_APNG_SIZE_BYTES", 10 * 1024 * 1024)


FRONTEND_BASE_DOMAIN = env.str("FRONTEND_BASE_DOMAIN", "https://kapi.bar")

CACHEOPS_REDIS = env.str(
    "CACHEOPS_REDIS", default="redis://kapibara-monolith-redis:6379/3"
)

CACHEOPS = {
    "comments.Comment": {"timeout": 60 * 60 * 24},
    "comments.CommentVote": {"timeout": 60 * 60 * 24},
    "posts.Post": {"timeout": 60 * 60 * 24},
    "posts.PostVote": {"timeout": 60 * 60 * 24},
    "posts.PostView": {"timeout": 60 * 60 * 24},
    "users.*": {"timeout": 60 * 60 * 24},
}
CACHEOPS_DEGRADE_ON_FAILURE = True
