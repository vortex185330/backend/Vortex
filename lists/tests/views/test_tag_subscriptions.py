import pytest
from funcy import lpluck, lpluck_attr
from rest_framework import status

from lists.models import TagSubscription
from lists.tests.factories import TagSubscriptionFactory
from lists.tests.mixins import ListsApiActionsMixin
from posts.choices import PostFeedType, PostStatus
from posts.tests.factories import PostFactory
from posts.tests.mixins import PostsApiActionsMixin
from tags.tests.factories import TagFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestTagSubscriptions(ListsApiActionsMixin, PostsApiActionsMixin):
    def setup_method(self):
        self.subscriber = UserPublicFactory()
        self.tag_to_subscribe = TagFactory()

    def test_user_subscribe_to_tag(self, authed_api_client):
        response = self._create_tag_subscription(
            authed_api_client(self.subscriber), tag=self.tag_to_subscribe
        )
        assert response.status_code == status.HTTP_201_CREATED, response.content.decode()
        result = response.json()

        db_subscriptions = TagSubscription.objects.filter(
            subscriber=self.subscriber, tag=self.tag_to_subscribe
        )
        db_subscription = db_subscriptions.first()
        assert db_subscriptions.count() == 1

        assert result["uuid"] == str(db_subscription.uuid)
        assert result["tag"] == self.tag_to_subscribe.name

    def test_user_subscribe_to_the_same_tag(self, authed_api_client):
        db_subscription = TagSubscriptionFactory(
            subscriber=self.subscriber, tag=self.tag_to_subscribe
        )
        response = self._create_tag_subscription(
            authed_api_client(self.subscriber), tag=self.tag_to_subscribe
        )
        assert response.status_code == status.HTTP_201_CREATED, response.content.decode()
        result = response.json()

        assert result["uuid"] == str(db_subscription.uuid)
        assert result["tag"] == self.tag_to_subscribe.name

    def test_user_cannot_subscribe_to_non_existing_tag(self, authed_api_client):
        response = self._create_tag_subscription(
            authed_api_client(self.subscriber),
            tag=TagFactory.build(name="non-existing-tag"),
        )
        assert (
            response.status_code == status.HTTP_400_BAD_REQUEST
        ), response.content.decode()

    def test_anon_user_cannot_subscribe(self, anon_api_client):
        response = self._create_tag_subscription(
            anon_api_client(), tag=self.tag_to_subscribe
        )
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
        ), response.content.decode()

    def test_user_can_list_own_tag_subscriptions(self, authed_api_client):
        subscriptions = TagSubscriptionFactory.create_batch(
            size=3, subscriber=self.subscriber
        )
        response = self._get_tag_subscriptions(authed_api_client(self.subscriber))
        assert response.status_code == status.HTTP_200_OK, response.content.decode()
        result = response.json()

        sorted_subscriptions = sorted(subscriptions, key=lambda s: s.tag.name)
        sorted_subscriptions_from_api = sorted(result, key=lambda s: s["tag"])

        assert list(map(str, lpluck_attr("uuid", sorted_subscriptions))) == lpluck(
            "uuid", sorted_subscriptions_from_api
        )

    def test_anon_user_cannot_list_tag_subscriptions(self, anon_api_client):
        response = self._get_tag_subscriptions(anon_api_client())
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
        ), response.content.decode()

    def test_user_can_unsubscribe_from_tag(self, authed_api_client):
        subscription = TagSubscriptionFactory(
            subscriber=self.subscriber, tag=self.tag_to_subscribe
        )
        response = self._delete_tag_subscription(
            authed_api_client(self.subscriber), subscription
        )
        assert (
            response.status_code == status.HTTP_204_NO_CONTENT
        ), response.content.decode()
        assert not TagSubscription.objects.filter(
            subscriber=self.subscriber, tag=self.tag_to_subscribe
        ).exists()

    def test_user_cant_unsubscribe_from_other_tag_subscription(self, authed_api_client):
        subscription = TagSubscriptionFactory(tag=self.tag_to_subscribe)
        response = self._delete_tag_subscription(
            authed_api_client(self.subscriber), subscription
        )
        assert (
            response.status_code == status.HTTP_404_NOT_FOUND
        ), response.content.decode()

    @pytest.mark.parametrize(
        "post_status",
        (
            PostStatus.DRAFT,
            PostStatus.DELETED,
            PostStatus.PUBLISHED,
        ),
    )
    @pytest.mark.parametrize("logged_in", (True, False))
    def test_user_can_see_posts_from_subscribed_tags_only(
        self, logged_in, post_status, authed_api_client, anon_api_client
    ):
        subscription = TagSubscriptionFactory(
            subscriber=self.subscriber, tag=self.tag_to_subscribe
        )
        posts = PostFactory.create_batch(
            size=2, tags=[subscription.tag], status=post_status
        )
        PostFactory.create_batch(size=2, status=PostStatus.PUBLISHED)

        sorted_posts_uuid = list(
            map(
                str,
                lpluck_attr(
                    "uuid", sorted(posts, key=lambda p: p.created_at, reverse=True)
                ),
            )
        )

        response = self._fetch_posts_feed(
            authed_api_client(self.subscriber) if logged_in else anon_api_client(),
            {"feed_type": PostFeedType.SUBSCRIPTIONS},
        )
        result = response.json()
        assert response.status_code == status.HTTP_200_OK, response.content.decode()

        if post_status in (PostStatus.DRAFT, PostStatus.DELETED) or logged_in is False:
            assert len(result["results"]) == 0
            return

        posts_from_api = lpluck("uuid", result["results"])

        assert sorted_posts_uuid == posts_from_api
