import pytest
from rest_framework import status
from rest_framework.reverse import reverse

from tags.api.v1.views import TagsViewSet
from tags.tests.factories import TagCategory, TagFactory
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestTagsAutosuggest:
    def setup_method(self):
        self.tag_to_suggest_1 = TagFactory(name="Капибара", usages=10)
        self.tag_to_suggest_2 = TagFactory(name="баранина", usages=100)
        self.tag_not_to_suggest = TagFactory(
            name="Капитан", category=TagCategory(allowed_to_add_manually=False)
        )

    @pytest.mark.parametrize(
        "search,expected_results",
        (
            ("бар", ["tag_to_suggest_2", "tag_to_suggest_1"]),
            ("а", ["tag_to_suggest_2", "tag_to_suggest_1"]),
            ("НИНА", ["tag_to_suggest_2"]),
            ("капитан", []),
            ("", []),
        ),
    )
    @pytest.mark.parametrize("logged_in", (False, True))
    def test_tags_suggested_properly(
        self, logged_in, search, expected_results, anon_api_client, authed_api_client
    ):
        throttle_scope = TagsViewSet.throttle_scope
        TagsViewSet.throttle_scope = None
        client = (
            authed_api_client(UserPublicFactory()) if logged_in else anon_api_client()
        )
        result = self._get_tags_autosuggestion(client, search)
        TagsViewSet.throttle_scope = throttle_scope

        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        response = result.json()

        results_from_api = [tag["name"] for tag in response]
        expected_results = [getattr(self, prop).name for prop in expected_results]
        assert results_from_api == expected_results

    def _get_tags_autosuggestion(self, client, search: str):
        return client.get(reverse("v1:tags:tags-list") + f"?s={search}")
