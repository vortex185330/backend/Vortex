from cacheops import cached_as
from django.http import QueryDict
from rest_framework.response import Response

from users.models import UserPublic


class CachedListViewMixin:
    def list(self, request, *args, **kwargs):  # noqa: CFQ004
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)

        @cached_as(
            *self.samples_for_list_cache(self.request.query_params),
            extra=self.extras_for_list_cache(
                self.request.user, self.request.query_params
            ),
        )
        def _get_result():
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return serializer.data

            serializer = self.get_serializer(queryset, many=True)
            return serializer.data

        if page is not None:
            return self.get_paginated_response(_get_result())
        return Response(_get_result())

    def samples_for_list_cache(self, query_params: QueryDict) -> list:
        return [self.get_queryset()]

    def extras_for_list_cache(
        self, user: UserPublic, query_params: QueryDict
    ) -> str | None:
        return None
