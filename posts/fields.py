from itertools import tee

from django.utils.encoding import force_str
from django_extensions.db.fields import AutoSlugField

from common.utils import generate_random_string
from posts.choices import PostStatus


class KapibaraPostAutoSlugField(AutoSlugField):
    def find_unique(self, model_instance, field, iterator, *args):
        first_iterator, second_iterator = tee(iterator, 2)
        generated_slug = next(first_iterator)
        if generated_slug:
            return super().find_unique(model_instance, field, second_iterator, *args)
        return None

    def create_slug(self, model_instance, add):
        if model_instance.status == PostStatus.DRAFT and not model_instance.slug:
            return None
        if model_instance.status == PostStatus.PUBLISHED:
            self.overwrite = False
        return super().create_slug(model_instance, add)

    def slug_generator(self, original_slug, start):
        yield original_slug
        for _ in range(0, self.max_unique_query_attempts):
            slug = original_slug
            end = f"{self.separator}{generate_random_string(6)}"
            end_len = len(end)
            if self.slug_len and len(slug) + end_len > self.slug_len:
                slug = slug[: self.slug_len - end_len]
                slug = self._slug_strip(slug)
            slug = f"{slug}{end}"
            yield slug
        raise RuntimeError(
            "max slug attempts for %s exceeded (%s)"
            % (original_slug, self.max_unique_query_attempts)
        )

    def pre_save(self, model_instance, add):
        slug = self.create_slug(model_instance, add)
        if slug is None:
            return slug
        return force_str(slug)
