from django.db.models import TextChoices


class NotificationCategories(TextChoices):
    USER_MENTION = "user_mention", "Упоминание пользователя"
    POST_MENTION = "post_mention", "Упоминание поста"
    COMMENT_MENTION = "comment_mention", "Упоминание комментария"
    INVITE = "invite", "Приглашение"
    UPDATE = "update", "Обновление"
    EVENT = "events", "Событие"
    SERVICE = "service", "Сервис"
    MODERATION = "moderation", "Модерация"
    USER_EVENT = "user_events", "События пользователя"


class NotificationTypes(TextChoices):
    WEB = "web", "Web Notification"


TYPE_CHOICES = [
    # ('email', 'Email'),
    # ('tg', 'Telegram'),
    # ('push', 'Push Notification'),
    ("web", "Web Notification"),
]
