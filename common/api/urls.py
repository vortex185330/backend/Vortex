from django.urls import path

from .views import sitemap_generator, sitemap_index

app_name = "common"

urlpatterns = [
    path("sitemap-index.xml", sitemap_index, name="sitemap_index"),
    path(
        "sitemap/<str:category>/<int:index>.xml", sitemap_generator, name="sitemap_pager"
    ),
]
