from django.contrib.admin.apps import AdminConfig


class KapibaraAdminOTPConfig(AdminConfig):
    default_site = "core_app.admin.KapibaraOTPAdmin"


class KapibaraAdminConfig(AdminConfig):
    default_site = "core_app.admin.KapibaraAdmin"
