import datetime

import pytest
from django.utils.timezone import make_aware
from funcy import lpluck, lpluck_attr
from rest_framework import status

from posts.choices import PostFeedType, PostStatus
from posts.tests.factories import PostFactory, PostViewFactory
from posts.tests.mixins import PostsApiActionsMixin
from users.tests.factories import UserPublicFactory


@pytest.mark.django_db
class TestViewedFeed(PostsApiActionsMixin):
    def setup_method(self):
        self.viewed_posts_by_viewer_1 = PostFactory.create_batch(
            size=2, status=PostStatus.PUBLISHED
        )
        self.viewed_posts_by_viewer_2 = PostFactory.create_batch(
            size=2, status=PostStatus.PUBLISHED
        )
        self.viewer_1, self.viewer_2 = UserPublicFactory.create_batch(size=2)

    def test_viewed_post_appears_in_viewed_feed(self, authed_api_client):
        # Просматриваем посты первым юзером
        client_1 = authed_api_client(self.viewer_1)
        self._view_all_posts(client_1, self.viewed_posts_by_viewer_1)
        # Проверяем, что лента просмотров первого юзера показывает только его просмотры
        self._assert_viewed_posts(
            client_1, self.viewed_posts_by_viewer_1, "User 1 has wrong viewed posts!"
        )
        # Просматриваем посты вторым юзером
        client_2 = authed_api_client(self.viewer_2)
        self._view_all_posts(client_2, self.viewed_posts_by_viewer_2)
        # Проверяем, что лента просмотров второго юзера показывает только его просмотры
        self._assert_viewed_posts(
            client_2, self.viewed_posts_by_viewer_2, "User 2 has wrong viewed posts!"
        )

    def test_draft_and_deleted_posts_are_not_showing_in_viewed_feed(
        self, authed_api_client
    ):
        # Помечаем один из постов удаленным, второй черновиком и проверяем что они пропали
        post_1, post_2 = self.viewed_posts_by_viewer_1
        post_1.status = PostStatus.DRAFT
        post_1.save()
        post_2.status = PostStatus.DELETED
        post_2.save()
        self._assert_viewed_posts(
            authed_api_client(self.viewer_1),
            [],
            "Deleted and draft posts shouldn't be in result!",
        )

    def test_anon_user_has_empty_viewed_feed(self, anon_api_client):
        # Для неавторизованного юзера ответ должен быть пустым
        self._assert_viewed_posts(
            anon_api_client(),
            [],
            "Anon user shouldn't see any posts",
        )

    def test_viewed_posts_sorted_by_viewed_timestamp_not_published(
        self, authed_api_client
    ):
        post_new, post_old = self.viewed_posts_by_viewer_1

        post_new.published_at = make_aware(datetime.datetime(2023, 10, 10))
        post_new.save()

        post_old.published_at = make_aware(datetime.datetime(2023, 10, 1))
        post_old.save()

        # Просматриваем старый пост, он должен быть первым в выводе просмотров
        post_view_new = PostViewFactory(
            post=post_old,
            user=self.viewer_1,
        )
        post_view_new.viewed_at = make_aware(datetime.datetime(2023, 10, 20))
        post_view_new.save()

        post_view_old = PostViewFactory(
            post=post_new,
            user=self.viewer_1,
        )
        post_view_old.viewed_at = make_aware(datetime.datetime(2023, 10, 15))
        post_view_old.save()

        result = self._get_viewed_posts(authed_api_client(self.viewer_1))
        assert result.status_code == status.HTTP_200_OK, result.content.decode()

        data = result.json()

        assert lpluck("uuid", data["results"]) == [
            str(post_view_new.post.uuid),
            str(post_view_old.post.uuid),
        ]

    def _assert_viewed_posts(self, client, expected_posts, assertion_error):
        result = self._get_viewed_posts(client)
        assert result.status_code == status.HTTP_200_OK, result.content.decode()
        data = result.json()
        assert set(lpluck("uuid", data["results"])) == set(
            map(str, lpluck_attr("uuid", expected_posts))
        ), assertion_error

    def _view_all_posts(self, client, posts):
        for post in posts:
            self._get_post(client, post)

    def _get_viewed_posts(self, client):
        return self._fetch_posts_feed(client, {"feed_type": PostFeedType.VIEWED})
