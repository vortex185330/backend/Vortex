import time
from typing import Annotated

from django.conf import settings
from funcy import count_by
from pydantic import Field, model_validator
from pydantic_core import PydanticCustomError

from common.content.blocks.context import ContentBaseModel
from common.content.blocks.image import BlockImage
from common.content.blocks.lists import BlockList
from common.content.blocks.paragraph import BlockParagraph
from common.content.blocks.quote import BlockQuote
from common.content.blocks.video import BlockVideo

ContentBlocks = Annotated[
    BlockImage | BlockList | BlockParagraph | BlockQuote | BlockVideo,
    Field(..., discriminator="type_"),
]


class BaseContentBlocks(ContentBaseModel):
    blocks: list[ContentBlocks] = Field(default_factory=list)
    time: int = Field(default_factory=lambda: int(time.time() * 1000))
    version: str = Field("", max_length=20)
    min_total_blocks: int = Field(0, exclude=True)
    max_total_blocks: int = Field(0, exclude=True)
    max_html_blocks: int = Field(0, exclude=True)
    max_img_blocks: int = Field(0, exclude=True)
    max_video_blocks: int = Field(0, exclude=True)
    should_validate: bool = Field(True, exclude=True)

    @model_validator(mode="after")
    def validate_data(self):
        if not self.should_validate:
            return self
        blocks_count = count_by(type, self.blocks)
        total_blocks_count = sum(blocks_count.values())
        if total_blocks_count < self.min_total_blocks:
            raise PydanticCustomError(
                "min_blocks_required",
                "Число элементов в списке должно быть как минимум "
                "{min_total_blocks}, а не {blocks_count}",
                {
                    "min_total_blocks": self.min_total_blocks,
                    "blocks_count": total_blocks_count,
                },
            )
        if blocks_count[BlockImage] > self.max_img_blocks:
            raise PydanticCustomError(
                "max_images_reached",
                "Можно добавить не более {max_images} изображений",
                {"max_images": self.max_img_blocks},
            )

        if blocks_count[BlockVideo] > self.max_video_blocks:
            raise PydanticCustomError(
                "max_videos_reached",
                "Можно добавить не более {max_videos} видео",
                {"max_videos": self.max_video_blocks},
            )

        html_blocks_count = sum(
            [
                blocks_count[BlockParagraph],
                blocks_count[BlockList],
                blocks_count[BlockQuote],
            ]
        )
        if html_blocks_count > self.max_html_blocks:
            raise PydanticCustomError(
                "max_text_block_reached",
                "Можно добавить не более {max_html_blocks} блоков текста",
                {"max_html_blocks": self.max_html_blocks},
            )
        return self


class PostContentBlocks(BaseContentBlocks):
    min_total_blocks: int = Field(1, exclude=True)
    max_total_blocks: int = Field(
        settings.CONTENT_POST_BLOCK_MAX_TOTAL_ITEMS, exclude=True
    )
    max_html_blocks: int = Field(settings.CONTENT_POST_BLOCK_MAX_HTML_ITEMS, exclude=True)
    max_img_blocks: int = Field(settings.CONTENT_POST_BLOCK_IMG_MAX_ITEMS, exclude=True)
    max_video_blocks: int = Field(
        settings.CONTENT_POST_BLOCK_VIDEO_MAX_ITEMS, exclude=True
    )


class CommentContentBlocks(BaseContentBlocks):
    min_total_blocks: int = Field(1, exclude=True)
    max_total_blocks: int = Field(
        settings.CONTENT_COMMENT_BLOCK_MAX_TOTAL_ITEMS, exclude=True
    )
    max_html_blocks: int = Field(
        settings.CONTENT_COMMENT_BLOCK_MAX_HTML_ITEMS, exclude=True
    )
    max_img_blocks: int = Field(
        settings.CONTENT_COMMENT_BLOCK_IMG_MAX_ITEMS, exclude=True
    )
    max_video_blocks: int = Field(
        settings.CONTENT_COMMENT_BLOCK_VIDEO_MAX_ITEMS, exclude=True
    )
