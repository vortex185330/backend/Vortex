from rest_framework import serializers


class SimpleMessageSerializer(serializers.Serializer):
    message = serializers.CharField()
