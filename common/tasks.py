import logging
import uuid
from typing import Optional

from celery import shared_task
from django.conf import settings
from django.db.models import Count
from django.utils import timezone

from posts.models import Post
from posts.selectors import (
    apply_exclude_tags,
    get_restricted_tag_names_for_unautorized_and_less_18,
)
from tags.models import Tag
from users.models import UserPublic

from .constants import FeedsURL, SiteMapUrlCategories, TaskStatus
from .models import SitemapEntry, SitemapTask

# from django_celery_beat.models import IntervalSchedule, PeriodicTask


logger = logging.getLogger(__name__)


# Создаем задачу с периодическим интервалом
# schedule, created = IntervalSchedule.objects.get_or_create(
#    every=5,  # период (например, каждые 5 минут)
#    period=IntervalSchedule.MINUTES,  # в минутах
# )

# Проверяем, существует ли уже периодическая задача с таким именем
# task_name = "SitemapPeriodicTask2"
# task, task_created = PeriodicTask.objects.get_or_create(
#    name=task_name,
#    defaults={
#        "interval": schedule,
#        "task": "common.tasks.update_sitemap",
#    },
# )

# Если задача уже существует, обновляем необходимые поля
# if not task_created:
#    task.interval = schedule
#    task.task = "common.tasks.update_sitemap"
#    task.save()


def list_splitter(objs_with_link: list, chunk_count=4) -> list[list]:
    """
    Splits a list of objects (tags, usernames, posts) into a specified number of chunks.
    This function takes a list of objects and divides it into `chunk_count`
    evenly distributed parts (as much as possible). Each chunk can then be
    processed separately, for example, using Celery.
    Returns A list of chunks, where each chunk is a sublist of objects.
    """
    avg_count_in_chunk = len(objs_with_link) / float(chunk_count)
    chunks = []
    last = 0.0

    while last < len(objs_with_link):
        entity = objs_with_link[int(last) : int(last + avg_count_in_chunk)]
        chunks.append(entity)
        last += avg_count_in_chunk

    return chunks


@shared_task
def parse_feeds(task_uid: str) -> None:
    """
    Process array of tag ids (names) to sitemap entries

    Args:
        task_uid (str): The task uid associated with the current operation.
    """
    logger.info("Processing feeds...")
    for feed in FeedsURL:
        try:
            logger.info(f"Creating feed entry with task_uid={task_uid}")

            entry, created = SitemapEntry.objects.get_or_create(
                url=f"{settings.FRONTEND_BASE_DOMAIN}/{feed}",
                category=SiteMapUrlCategories.FEED,
                task_uid=task_uid,
            )
            if created:
                logger.info(f"Created feed entry: {entry.url}")
            else:
                logger.info(f"Feed entry already exists: {entry.url}")
        except Exception as e:
            logger.error(f"Error creating feed entry for {feed}: {e}")


@shared_task
def parse_profiles(task_uid: str, profile_ids: list) -> None:
    """
    Process array of profile ids (external_user_uid) to sitemap entries

    Args:
        task_uid (str): The task uid associated with the current operation.
        profile_ids (list): List of profile external_user_uid for processing
    """
    logger.info("Processing profiles...")
    profiles = UserPublic.objects.filter(external_user_uid__in=profile_ids)
    logger.info(f"Profiles count: {profiles.count()}")
    for profile in profiles:
        entry, created = SitemapEntry.objects.get_or_create(
            url=profile.get_absolute_url(),
            category=SiteMapUrlCategories.USER,
            task_uid=task_uid,
        )
        if created:
            logger.info(f"Created profile entry: {entry.url}")


@shared_task
def parse_posts(task_uid: str, post_ids: list) -> None:
    """
    Process array of post ids (uuid) to sitemap entries

    Args:
        task_uid (str): The task uid associated with the current operation.
        post_ids (list): List of post ids for processing
    """
    logger.info("Processing posts...")
    posts = Post.objects.filter(uuid__in=post_ids)
    logger.info(f"Posts count: {posts.count()}")
    for post in posts:
        entry, created = SitemapEntry.objects.get_or_create(
            url=post.get_absolute_url(),
            category=SiteMapUrlCategories.POST,
            task_uid=task_uid,
        )
        if created:
            logger.info(f"Created post entry: {entry.url}")


@shared_task
def parse_tags(task_uid: str, tag_ids: list) -> None:
    """
    Process array of tag ids (name) to sitemap entries

    Args:
        task_uid (str): The task uid associated with the current operation.
        tag_ids (list): List of tag names for processing
    """
    logger.info("Processing tags...")
    tags = Tag.objects.filter(name__in=tag_ids).annotate(post_count=Count("post"))
    logger.info(f"Tag count: {tags.count()}")
    for tag in tags:
        exist_data = tag.post_count > 0  # Если у тега есть связанные посты
        entry, created = SitemapEntry.objects.get_or_create(
            url=tag.get_absolute_url(),
            category=SiteMapUrlCategories.TAG,
            defaults={"exist_data": exist_data},
            task_uid=task_uid,
        )
        if created:
            logger.info(f"Created tag entry: {entry.url}")


@shared_task
def tag_tasks(task_record: SitemapTask, tag_query) -> None:
    """
    Splits a list of tags and schedules Celery tasks to process them.

    Args:
        task_record (SitemapTask): The task record associated with the current operation.
        tag_query: The queryset of tags to process.
    """
    if task_record.tags:
        list_all_tag = list(tag_query.values_list("name", flat=True))
        for list_tag in list_splitter(list_all_tag):
            parse_tags.apply_async(
                kwargs={
                    "task_uid": task_record.task_uid,
                    "tag_ids": list_tag,
                }
            )


@shared_task
def profile_task(task_record: SitemapTask, user_query) -> None:
    """
    Splits a list of user profiles and schedules Celery tasks to process them.

    Args:
        task_record (SitemapTask): The task record associated with the current operation.
        user_query: The queryset of user profiles to process.
    """
    if task_record.profiles:
        list_all_user = list(user_query.values_list("external_user_uid", flat=True))
        for list_user in list_splitter(list_all_user):
            parse_profiles.apply_async(
                kwargs={
                    "task_uid": task_record.task_uid,
                    "profile_ids": list_user,
                }
            )


@shared_task
def post_tasks(task_record: SitemapTask, post_query) -> None:
    """
    Splits a list of posts and schedules Celery tasks to process them.

    Args:
        task_record (SitemapTask): The task record associated with the current operation.
        post_query: The queryset of posts to process.
    """
    if task_record.posts:
        list_all_post = list(post_query.values_list("uuid", flat=True))
        for list_post in list_splitter(list_all_post):
            parse_posts.apply_async(
                kwargs={
                    "task_uid": task_record.task_uid,
                    "post_ids": list_post,
                }
            )


@shared_task
def update_sitemap(task_uid: Optional[str] = None, with_clean: bool = False) -> None:
    """
    Updates the sitemap by creating entries for feeds, posts, profiles, and tags.

    This task creates or updates a `SitemapTask` record, retrieves the required
    data based on the task's configuration, and schedules subtasks to process
    each type of data.

    Args:
        task_uid (Optional[str]): The unique identifier of the task.
            If None, a new task record is created. Defaults to None.
        with_clean (bool): Whether to clean old records before processing.
        Defaults to False.
    """
    logger.info("Processing update_sitemap...")
    logger.info(f"task_uid: {task_uid}, with_clean: {with_clean}")
    if task_uid is None:
        task_uid = str(uuid.uuid4())
        logger.info(f"Create new task_uid: {task_uid}")

    task_record, _ = SitemapTask.objects.get_or_create(
        task_uid=task_uid,
        defaults={
            "feeds": True,
            "profiles": False,
            "posts": False,
            "tags": False,
            "all_time": False,
        },
    )
    task_record.status = TaskStatus.RUNNING
    task_record.save()
    logger.info(f"task_record: {task_record}")
    logger.info(f"task_uid being used: {task_record.task_uid}")
    logger.info(f"Frontend base domain: {settings.FRONTEND_BASE_DOMAIN}")

    try:
        # TODO: temporary turn off clean up
        # if with_clean:
        #     logger.info("Cleaning old records...")
        #     SitemapEntry.objects.all().delete()

        post_query = apply_exclude_tags(Post.published, None)
        user_query = UserPublic.objects.filter(is_banned=False)
        restricted_tag_names = get_restricted_tag_names_for_unautorized_and_less_18()
        tag_query = Tag.objects.exclude(name__in=restricted_tag_names)

        if not task_record.all_time:
            last_visit_time = (
                SitemapEntry.objects.order_by("-last_visited").last().last_visited
            )
            post_query = post_query.filter(published_at__gte=last_visit_time)
            user_query = user_query.filter(created_at__gte=last_visit_time)

        if task_record.feeds:
            parse_feeds(task_record.task_uid)
            # parse_feeds.apply_async(args=[task_record])
        post_tasks(task_record, post_query)
        profile_task(task_record, user_query)
        tag_tasks(task_record, tag_query)

    except Exception as e:
        logger.error(f"Error occurred: {e}")
        task_record.status = TaskStatus.ERROR
        task_record.save()
        return

    task_record.status = TaskStatus.COMPLETED
    task_record.finished_at = timezone.now()
    task_record.save()
    logger.info("Finish update_sitemap successful")
